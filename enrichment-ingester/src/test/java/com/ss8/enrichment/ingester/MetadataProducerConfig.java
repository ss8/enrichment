package com.ss8.enrichment.ingester;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import com.ss8.metahub.shared.dto.MetadataWrapper;

@Configuration
public class MetadataProducerConfig {

	@Value("${spring.kafka.bootstrap-servers}")
	private String bootstrapServers;
	
	@Value("${spring.kafka.producer.retries}")
	private int retries;
	
	@Value("${spring.kafka.producer.batch-size}")
	private int batchSize;
	
	@Value("${spring.kafka.producer.linger-ms}")
	private long lingerInMs;
	
	@Value("${spring.kafka.producer.buffer-memory}")
	private long bufferMemory;

	@Bean
	public Map<String, Object> metadataProducerConfigs() {
		Map<String, Object> props = new HashMap<>();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		props.put(ProducerConfig.RETRIES_CONFIG, retries);
		props.put(ProducerConfig.BATCH_SIZE_CONFIG, batchSize);
		props.put(ProducerConfig.LINGER_MS_CONFIG, lingerInMs);
		props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, bufferMemory);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
		return props;
	}

	@Bean
	public ProducerFactory<String, MetadataWrapper> metadataProducerFactory() {
		return new DefaultKafkaProducerFactory<>(metadataProducerConfigs());
	}

	@Bean
	public KafkaTemplate<String, MetadataWrapper> metadataKafkaTemplate() {
		return new KafkaTemplate<>(metadataProducerFactory());
	}

	@Bean
	public MetadataProducer metadataSender() {
		return new MetadataProducer();
	}
}