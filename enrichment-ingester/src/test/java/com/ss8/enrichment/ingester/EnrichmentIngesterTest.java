package com.ss8.enrichment.ingester;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.ss8.enrichment.ingester.dao.ImportStatusUpdateDao;
import com.ss8.metahub.shared.dto.GeoPoint;
import com.ss8.metahub.shared.dto.ImportStatus;
import com.ss8.metahub.shared.dto.Metadata;
import com.ss8.metahub.shared.dto.MetadataWrapper;

@EmbeddedKafka(partitions = 10, topics = { "ENRICHMENT-TOPIC" })
public class EnrichmentIngesterTest extends ElasticBase {

	@Autowired
	private MetadataProducer metadataProducer;

	@Autowired
	private EmbeddedKafkaBroker embeddedKafka;
	
	@Value("${enrichment.elastic.readIndexName:enrichment}")
	private String readIndexName;

	@Value("${enrichment.elastic.writeIndexName:enrichment-index}")
	private String writeIndexName;

	@Value("${kafka.enrichment.topic}")
	private String topic;
	
	@Autowired
	ImportStatusUpdateDao dao;

	@Test(description = "Test by sending single enrichment record and see if we can ingest into elastic search.")
	public void ingestToElasticSearchTest1() throws Exception {
		metadataProducer.send(this.createMetadata(1));
		Thread.sleep(10000);
		SearchRequest searchRequest = new SearchRequest(this.readIndexName);
		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		searchSourceBuilder.size(5000); // max is 10000
		searchRequest.source(searchSourceBuilder);
		SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
		SearchHits hits = searchResponse.getHits();
		SearchHit[] searchHits = hits.getHits();
		int totalCount = searchHits.length;
		for (SearchHit hit : searchHits) {
			System.out.println(hit.getSourceAsString());
		}
		assertThat(totalCount).isEqualTo(1);
		this.deleteDocs(this.writeIndexName);
	}

	@Test(description = "Test by sending multiple enrichment records and see if we can ingest into elastic search.")
	public void ingestToElasticSearchTest2() throws Exception {
		for (int index = 0; index < 1000; index++) {
			metadataProducer.send(this.createMetadata(index + 1));
		}
		Thread.sleep(10000);
		SearchRequest searchRequest = new SearchRequest(this.readIndexName);
		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		searchSourceBuilder.size(5000); // max is 10000
		searchRequest.source(searchSourceBuilder);
		SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
		SearchHits hits = searchResponse.getHits();
		SearchHit[] searchHits = hits.getHits();
		int totalCount = searchHits.length;
		for (SearchHit hit : searchHits) {
			System.out.println(hit.getSourceAsString());
		}
		assertThat(totalCount).isEqualTo(1000);
		this.deleteDocs(this.writeIndexName);
	}
	
	@Test(description = "Test by sending multiple enrichment records and some of them have wrong azimuth value and see those records are not in elastic search.")
	public void ingestToElasticSearchTest3() throws Exception {
		for (int index = 0; index < 10; index++) {
			metadataProducer.send(this.createMetadata(index + 1, 11, 10.0));
		}
		for (int index = 10; index < 20; index++) {
			metadataProducer.send(this.createMetadata(index + 1, 11, 380.0));
		}
		Thread.sleep(10000);
		SearchRequest searchRequest = new SearchRequest(this.readIndexName);
		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		searchSourceBuilder.size(5000); // max is 10000
		searchRequest.source(searchSourceBuilder);
		SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
		SearchHits hits = searchResponse.getHits();
		SearchHit[] searchHits = hits.getHits();
		int totalCount = searchHits.length;
		for (SearchHit hit : searchHits) {
			System.out.println(hit.getSourceAsString());
		}
		assertThat(totalCount).isEqualTo(10);
		System.out.println("Import Status -> " + dao.getDataImportStatus(11));
		assertThat(dao.getDataImportStatus(11)).isEqualTo(ImportStatus.INGEST_FAILED.toString());
		System.out.println("Import Failed Records -> " + dao.getDataImportFailedRecords(11));
		assertThat(dao.getDataImportFailedRecords(11)).isEqualTo(10);
		this.deleteDocs(this.writeIndexName);
	}

	private MetadataWrapper createMetadata(final int id) {
		Metadata metadata = new Metadata();
		metadata.setId("" + id);
		metadata.setDatasourceId(1);
		metadata.setEventType("Enrichment");
		metadata.setDatasourceName("BSA");
		metadata.setDatasourceType("BSA");
		metadata.setImportId(id % 10);
		metadata.setImportName("Import-1");
		HashMap<String, String> metadataTextMap = new HashMap<String, String>();
		metadataTextMap.put("cellType", "CGI");
		metadataTextMap.put("cellId", "452-01-10043-13513");
		metadataTextMap.put("physicalAddress", "Ha Tinh-Loc Ha-Tram Vien Thong Thach Chau, Loc Ha, Ha Tinh");
		metadata.setMetadataTextMap(metadataTextMap);
		HashMap<String, Double> metadataDoubleMap = new HashMap<String, Double>();
		metadataDoubleMap.put("azimuth", 10.0);
		metadataDoubleMap.put("width", 10.0);
		metadataDoubleMap.put("range", 10.0);
		metadata.setMetadataDoubleMap(metadataDoubleMap);
		List<GeoPoint> geoPoints = new LinkedList<GeoPoint>();
		geoPoints.add(new GeoPoint(19.81111, 105.77941));
		metadata.setEventLocations(geoPoints);
		
		MetadataWrapper metadataWrapper = new MetadataWrapper();
		metadataWrapper.setMetadata(metadata);
		metadataWrapper.setOperationType(MetadataWrapper.TYPE_CREATE);
		
		return metadataWrapper;
	}
	
	private MetadataWrapper createMetadata(final int id, final long importId, double azimuth) {
		Metadata metadata = new Metadata();
		metadata.setId("" + id);
		metadata.setDatasourceId(1);
		metadata.setEventType("Enrichment");
		metadata.setDatasourceName("BSA");
		metadata.setDatasourceType("BSA");
		metadata.setImportId(importId);
		metadata.setImportName("Import-" + importId);
		HashMap<String, String> metadataTextMap = new HashMap<String, String>();
		metadataTextMap.put("cellType", "CGI");
		metadataTextMap.put("cellId", "452-01-10043-13513");
		metadataTextMap.put("physicalAddress", "Ha Tinh-Loc Ha-Tram Vien Thong Thach Chau, Loc Ha, Ha Tinh");
		metadata.setMetadataTextMap(metadataTextMap);
		HashMap<String, Double> metadataDoubleMap = new HashMap<String, Double>();
		metadataDoubleMap.put("azimuth", azimuth);
		metadataDoubleMap.put("width", 10.0);
		metadataDoubleMap.put("range", 10.0);
		metadata.setMetadataDoubleMap(metadataDoubleMap);
		List<GeoPoint> geoPoints = new LinkedList<GeoPoint>();
		geoPoints.add(new GeoPoint(19.81111, 105.77941));
		metadata.setEventLocations(geoPoints);
		
		MetadataWrapper metadataWrapper = new MetadataWrapper();
		metadataWrapper.setMetadata(metadata);
		metadataWrapper.setOperationType(MetadataWrapper.TYPE_CREATE);
		
		return metadataWrapper;
	}

	@AfterClass
	public void cleanup() {
		this.embeddedKafka.destroy();
	}
}
