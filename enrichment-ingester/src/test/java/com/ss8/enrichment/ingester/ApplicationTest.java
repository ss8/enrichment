package com.ss8.enrichment.ingester;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Reporter;

@SpringBootTest(classes = Application.class)
@TestPropertySource(locations = "file:src/test/resources/application-test.properties")
public class ApplicationTest extends AbstractTestNGSpringContextTests {

	public static String ptH;
	public static final boolean IS_WINDOWS = System.getProperty("os.name").contains("Windows");

	public static String home = ptH + "\\src\\test\\java\\com\\ss8\\enrichment\\ingester";
	public static String dataHomeConfig = home + "\\data\\config/";
	public static String dataHomeRequest = home + "\\data\\request/";
	
	public ApplicationTest() {
		System.out.println(" User dir -> " + System.getProperty("user.dir"));
		System.out.println("Constructing ApplicationTest ...");
		EnvSetup.beChatty = true;
		ptH = EnvSetup.setSystemProps("enrichment-ingester");
		home = ptH + "\\src\\test\\java\\com\\ss8\\enrichment\\ingester";
		dataHomeConfig = home + "\\data\\config/";
		dataHomeRequest = home + "\\data\\request/";
	}

	public void copyFile(String from, String to) throws IOException {
		Path copied = Paths.get(to);

		Path originalPath = Paths.get(from);
		Files.copy(originalPath, copied, StandardCopyOption.REPLACE_EXISTING);
	}

	public static void setFinalStaticField(Class<?> classzz, String fieldName, Object value, int modifier)
			throws ReflectiveOperationException {
		Field field = classzz.getDeclaredField(fieldName);
		field.setAccessible(true);
		Field modifiers = Field.class.getDeclaredField("modifiers");
		modifiers.setAccessible(true);
		modifiers.setInt(field, field.getModifiers() & ~modifier);
		field.set(null, value);
	}
	
	public void print(String line) {
		System.out.println(line);
		 Reporter.log(line);
	}
}
