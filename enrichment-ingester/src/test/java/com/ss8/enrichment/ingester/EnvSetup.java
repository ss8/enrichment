package com.ss8.enrichment.ingester;

import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class EnvSetup {
	
	public static boolean beChatty = false;
	static final boolean IS_WINDOWS = System.getProperty("os.name").contains("Windows");
	static final String HOME = "ENRICHMENT_INGESTER_HOME";

	static String discoveredPath = null;

	public static String getPath( ) { return (discoveredPath); }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void setEnv(Map<String, String> newenv) {
		try {
			Class<?> processEnvironmentClass = Class.forName("java.lang.ProcessEnvironment");
			Field theEnvironmentField = processEnvironmentClass.getDeclaredField("theEnvironment");
			theEnvironmentField.setAccessible(true);
			Map<String, String> env = (Map<String, String>) theEnvironmentField.get(null);
			env.putAll(newenv);
			Field theCaseInsensitiveEnvironmentField = processEnvironmentClass.getDeclaredField("theCaseInsensitiveEnvironment");
			theCaseInsensitiveEnvironmentField.setAccessible(true);
			Map<String, String> cienv = (Map<String, String>) theCaseInsensitiveEnvironmentField.get(null);
			cienv.putAll(newenv);
		} catch (NoSuchFieldException e) {
			try {
				Class[] classes = Collections.class.getDeclaredClasses();
				Map<String, String> env = System.getenv();
				for (Class cl : classes) {
					if ("java.util.Collections$UnmodifiableMap".equals(cl.getName())) {
						Field field = cl.getDeclaredField("m");
						field.setAccessible(true);
						Object obj = field.get(env);
						Map<String, String> map = (Map<String, String>) obj;
						map.clear();
						map.putAll(newenv);
					}
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	private static boolean extractFusionHome(URL url, String component, String suffix, Map<String, String> newenv) 
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		boolean hasIt = false;
		String urlFile = url.getFile( );
		int iSuffix = urlFile.indexOf(component + '/' + suffix);
		
		if (iSuffix > 0) {
			// We found the component within the string, so advance past the component and '/', then chop
			String path = urlFile.substring(0, (iSuffix + component.length( ) + 1));
			if (path == null) path = "";
			discoveredPath = path;
			newenv.put(HOME,  path + "src/test/resources");
			Field fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
			fieldSysPath.setAccessible(true);
			fieldSysPath.set(null, null);
			setEnv(newenv);
			hasIt = true;
		}
		
		return (hasIt);
	}
	
	public static String setSystemProps(String component) {
		
		try {
			Map<String, String> newenv = new HashMap<String, String>();
			ClassLoader cl = ClassLoader.getSystemClassLoader();
			URL[] urls = ((URLClassLoader) cl).getURLs();
			if (beChatty) System.out.println("Number of class loader URLs: " + urls.length);
			for (URL url : urls) {
				if (beChatty) System.out.println(url.getFile());
				if (extractFusionHome(url, component, "build/resources/test/", newenv) || extractFusionHome(url, component, "build/classes/java/main/", newenv) || extractFusionHome(url, component, "bin/", newenv) ) break;
			}
			if ((discoveredPath != null) && IS_WINDOWS) discoveredPath = discoveredPath.substring(1);
		} catch (Exception ex) {
			System.out.println("Unable to determine " + HOME + " for " + component + " from loaded class items: " + ex);
		}
		
		return (discoveredPath);  // set in successful call to extractSAHome( )
	}
}
