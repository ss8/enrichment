package com.ss8.enrichment.ingester;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;

import com.ss8.metahub.shared.dto.MetadataWrapper;

public class MetadataProducer {

  private static final Logger LOGGER = LoggerFactory.getLogger(MetadataProducer.class);

  @Value("${kafka.enrichment.topic}")
  private String enrichmentTopic;

  @Autowired
  private KafkaTemplate<String, MetadataWrapper> kafkaTemplate;

  public void send(MetadataWrapper metadata) {
    LOGGER.debug("sending metadata='{}'", metadata.toString());
    kafkaTemplate.send(enrichmentTopic, metadata);
  }
}