package com.ss8.enrichment.ingester.config;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AbstractFactoryBean;
import org.springframework.context.annotation.Configuration;

import com.ss8.iql.query.ElasticQueryWrapper;
import com.ss8.iql.query.dto.ElasticServer;

@Configuration
public class ElasticSearchConfiguration extends AbstractFactoryBean<RestHighLevelClient> {

	private static final Logger LOG = LoggerFactory.getLogger(ElasticSearchConfiguration.class);

//	private ElasticQueryWrapper elasticWrapper;

	@Value("${enrichment.elastic.ip:localhost}")
	private String host;

	@Value("${enrichment.elastic.port:12913}")
	private int port;
	
	@Value("${elastic.customConfigEnabled:false}")
	private boolean customConfigEnabled;
	
	@Value("${elastic.customConfig.socketTimeout: 30000}")
	private int socketTimeout;
	
	@Value("${elastic.customConfig.connectionTimeout: 1000}")
	private int connectionTimeout;
	
	@Value("${elastic.customConfig.maxConnectionsPerRoute: 10}")
	private int maxConnectionsPerRoute;
	
	@Value("${elastic.customConfig.maxConnTotal: 30}")
	private int maxConnTotal;
	
	@Value("${elastic.customConfig.maxIdleTimeout: 30000}")
	private int maxIdleTimeout;

	private RestHighLevelClient restHighLevelClient;
	
	private static ElasticQueryWrapper eqw = null;

	@Override
	public boolean isSingleton() {
		return false;
	}

	public ElasticQueryWrapper getElasticInstance() {
		if(eqw != null) {
			return eqw;
		}
		eqw = ElasticQueryWrapper.getInstance();
		List<ElasticServer> servers = new ArrayList<>(); // list of servers ElasticServer
		ElasticServer methubInstance = new ElasticServer(this.host, this.port);
		servers.add(methubInstance);
		eqw.RegisterElasticServers(servers); // register the servers
		return eqw;
	}
	
	@Override
	public void destroy() {
		try {
			if (restHighLevelClient != null) {
				restHighLevelClient.close();
			}
		} catch (final Exception e) {
			LOG.error("Error closing ElasticSearch client: ", e);
		}
	}

	@Override
	public Class<RestHighLevelClient> getObjectType() {
		return RestHighLevelClient.class;
	}

	@Override
	public RestHighLevelClient createInstance() {
		return buildClient();
	}

	private RestHighLevelClient buildClient() {
		try {
			restHighLevelClient = new RestHighLevelClient(RestClient
					.builder(new HttpHost(this.host, this.port, null)));
		} catch (Exception e) {
			LOG.error("", e);
		}

		return restHighLevelClient;
	}

}
