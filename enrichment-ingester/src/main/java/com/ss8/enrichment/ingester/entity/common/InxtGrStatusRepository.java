package com.ss8.enrichment.ingester.entity.common;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InxtGrStatusRepository
		extends CrudRepository<InxtGrStatus, Long> {

	List<InxtGrStatus> findBySite(String site);
}
