package com.ss8.enrichment.ingester.config;

import java.util.Objects;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.ss8.metahub.shared.util.JasyptPasswordCryptor;
import com.zaxxer.hikari.HikariDataSource;


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
		basePackages = "com.ss8.enrichment.ingester.entity.common",
		entityManagerFactoryRef = "commonEntityManagerFactory",
		transactionManagerRef = "commonPlatformTransactionManager"
)
public class CommonJdbcConfiguration {
    static final Logger log = LoggerFactory.getLogger(CommonJdbcConfiguration.class);

	@Value("${spring.datasource.hikari.connection-timeout:30000}")
	protected long connectionTimeout;

	@Value("${spring.datasource.hikari.maxLifeTime:1800000}")
	protected int maxLifeTime;

	@Value("${spring.datasource.driver-class-name}")
	private String driver;

	@Value("${spring.datasource.common.password}")
	private String encodedPassword;

	@Value("${spring.datasource.common.url}")
	private String url;

	@Value("${spring.datasource.common.username}")
	private String username;
	
	@Value("${spring.datasource.common.hikari.maximumPoolSize:10}")
	private int commonMaxPoolSize;

//	@Primary
	@Bean(name = "commonDataSource")
	public DataSource commonDataSource() {
		String password = JasyptPasswordCryptor.getInstance()
				.decrypt(encodedPassword);
		if (log.isInfoEnabled()) {
			log.info("Database configuration: D=" + driver + "\nURL=" + url);
		}
		 
		HikariDataSource dataSource = new HikariDataSource();
		dataSource.setDriverClassName(driver);
		dataSource.setJdbcUrl(url);
		dataSource.setUsername(username);
		dataSource.setPassword(password);
		dataSource.addDataSourceProperty("cachePrepStmts", true);
		dataSource.addDataSourceProperty("prepStmtCacheSize", 25000);
		dataSource.addDataSourceProperty("prepStmtCacheSqlLimit", 20048);
		dataSource.addDataSourceProperty("useServerPrepStmts", true);
		dataSource.addDataSourceProperty("initializationFailFast", true);
		String poolName = "MYSQL_CONNECTION_POOL_COMMON";
		dataSource.setPoolName(poolName);
		dataSource.setMaximumPoolSize(this.commonMaxPoolSize);
		dataSource.setConnectionTimeout(this.connectionTimeout);
		dataSource.setMaxLifetime(this.maxLifeTime);

		return dataSource;		
	}

	@Primary
	@Bean
	public LocalContainerEntityManagerFactoryBean commonEntityManagerFactory(
			@Qualifier("commonDataSource")DataSource dataSourceSFW,
			EntityManagerFactoryBuilder builder) {
		return builder.dataSource(commonDataSource())
				.packages("com.ss8.enrichment.ingester.entity.common")
				.build();
	}

	@Bean
	public PlatformTransactionManager commonPlatformTransactionManager(
			@Qualifier("commonEntityManagerFactory") LocalContainerEntityManagerFactoryBean commonEntityManagerFactory) {
		return new JpaTransactionManager(Objects.requireNonNull(commonEntityManagerFactory.getObject()));
	}
	
//    @Bean(name = "jdbcCommonDBTemplate") 
//    public JdbcTemplate jdbcCommonTemplate(@Qualifier("dataSource")DataSource dataSourceSFW) {
//        return new JdbcTemplate(dataSourceSFW); 
//    } 
    
//    @Bean(name = "jdbcCommonDBNamedTemplate") 
//    public NamedParameterJdbcTemplate namedParameterCommonJdbcTemplate(@Qualifier("dataSource")DataSource dataSourceSFW){
//       return new NamedParameterJdbcTemplate(dataSourceSFW);
//    }
}
