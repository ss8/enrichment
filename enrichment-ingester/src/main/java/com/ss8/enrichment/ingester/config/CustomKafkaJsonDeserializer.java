package com.ss8.enrichment.ingester.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;

/*
 * Based on JsonDeserializer class in org.apache.kafka.connect.json package
 */

public class CustomKafkaJsonDeserializer<T> implements Deserializer<T> {
	private final ObjectMapper objectMapper = new ObjectMapper();
	private static final Log log = LogFactory.getLog(CustomKafkaJsonDeserializer.class);
	
	private Class<T> targetType;

	/**
	 * Default constructor needed by Kafka
	 */
	public CustomKafkaJsonDeserializer() {
	}
	
    public CustomKafkaJsonDeserializer(Class<T> targetType) {
        this.targetType = targetType;
    }

	@Override
	public T deserialize(String topic, byte[] data) {
        try {
            if (data == null) {
                return null;
            }
            return objectMapper.readValue(data, targetType);
        } catch (Exception e) {
            throw new SerializationException("Error deserializing JSON message", e);
        }
    }
}
