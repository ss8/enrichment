package com.ss8.enrichment.ingester.entity.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "status", "site" })
@Entity
@Table(name = "inxt_gr_status")
public class InxtGrStatus {
	@Id
	@Column(name = "id")
	@JsonIgnore
	private long id;

	@Column(name = "site")
	private String site;

	@JsonProperty("status")
	@Column(name = "site_status")
	private String siteStatus;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getSiteStatus() {
		return siteStatus;
	}

	public void setSiteStatus(String siteStatus) {
		this.siteStatus = siteStatus;
	}

}
