package com.ss8.enrichment.ingester.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.RoundRobinAssignor;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties.AckMode;

import com.ss8.enrichment.ingester.MetadataConsumer;
import com.ss8.iql.query.ElasticQueryWrapper;
import com.ss8.metahub.shared.dto.MetadataWrapper;

@Configuration
@EnableKafka
public class MetadataConsumerConfig {

	@Value("${spring.kafka.bootstrap-servers}")
	private String bootstrapServers;

	@Value("${spring.kafka.consumer.enable-auto-commit}")
	private String consumerEnableAutoCommit;

	@Value("${spring.kafka.consumer.auto-commit-interval}")
	private String consumerAutoCommitInterval;

	@Value("${spring.kafka.consumer.properties.session-timeout}")
	private String consumerSessionTimeout;

	@Value("${spring.kafka.consumer.enrichmentConsumer-group-id}")
	private String metadataConsumerGroupId;

	@Value("${spring.kafka.consumer.auto-offset-reset}")
	private String consumerAutoOffsetReset;

	@Value("${enrichment.ingester.concurrency:3}")
	private Integer listenerConcurrency;

	@Value("${spring.kafka.listener.poll-timeout}")
	private long listenerPollTimeout;

	@Value("${spring.kafka.consumer.properties.max-partition-fetch-bytes}")
	private String consumerMaxPartitionFetchSize;
	
	@Value("${enrichment.ingester.maxPollRecords:250}")
	private int consumerMaxPollRecords;
	
	public MetadataConsumerConfig() {}

	@Bean
	public Map<String, Object> metadataConsumerConfigs() {
		Map<String, Object> props = new HashMap<>();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, CustomKafkaJsonDeserializer.class);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, metadataConsumerGroupId);
		props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, consumerAutoOffsetReset);
		props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, consumerMaxPollRecords);
		props.put(ConsumerConfig.PARTITION_ASSIGNMENT_STRATEGY_CONFIG, RoundRobinAssignor.class.getName());
		return props;
	}

	@Bean
	public ConsumerFactory<String, MetadataWrapper> metadataConsumerFactory() {
		return new DefaultKafkaConsumerFactory<>(metadataConsumerConfigs(),
				new StringDeserializer(),
				new CustomKafkaJsonDeserializer<>(MetadataWrapper.class));
	}

	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, MetadataWrapper> kafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, MetadataWrapper> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(metadataConsumerFactory());
		factory.setConcurrency(listenerConcurrency);
		factory.setBatchListener(true);
		factory.getContainerProperties().setPollTimeout(listenerPollTimeout);
		factory.getContainerProperties().setAckMode(AckMode.MANUAL_IMMEDIATE);
		return factory;
	}

	@Bean
	public MetadataConsumer metadataReceiver() {
		return new MetadataConsumer();
	}
}