package com.ss8.enrichment.ingester;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.Security;

import org.bouncycastle.jcajce.provider.BouncyCastleFipsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.cassandra.CassandraDataAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"com.ss8.shared", "com.ss8.enrichment"})
@SpringBootApplication(scanBasePackages = {"com"}, exclude = {CassandraDataAutoConfiguration.class})
public class Application {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) throws UnknownHostException {
    	// Add Bouncy Castle FIPS provider
        Security.addProvider(new BouncyCastleFipsProvider());
        
    	String hostName = InetAddress.getLocalHost().getHostName();
    	System.setProperty("log.name", hostName);
        SpringApplication.run(Application.class, args);
        LOGGER.info("Starting Enrichment Ingester Application....");
    }
}
