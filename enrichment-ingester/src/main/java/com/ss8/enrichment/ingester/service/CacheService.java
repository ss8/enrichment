package com.ss8.enrichment.ingester.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.ss8.enrichment.ingester.entity.common.InxtGrStatus;
import com.ss8.enrichment.ingester.entity.common.InxtGrStatusRepository;

@Service
public class CacheService {

	@Autowired
	private InxtGrStatusRepository inxtGrStatusRepository;

	@Cacheable(cacheNames = "gr_cache", key = "#sitename")
	public String getCurrentSiteStatus(String sitename) {
		List<InxtGrStatus> grList = inxtGrStatusRepository.findBySite(sitename);
		if (grList.size() > 0) {
			return grList.get(0).getSiteStatus();
		}
		
		return null;
	}

}
