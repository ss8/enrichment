package com.ss8.enrichment.ingester.dao;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ss8.metahub.shared.dao.BaseJdbcDao;
import com.ss8.metahub.shared.dto.ImportStatus;

@Repository("importStatusUpdateDao")
public class ImportStatusUpdateDao extends BaseJdbcDao {

	private static final String UPDATE_STATUS_QUERY_IMPORT = "update ingest_dataimport set import_status = ?, updated_date = ?, error = ?   where id  = ?";

	private static final String SELECT_QUERY_IMPORT_STATUS = "select import_status from ingest_dataimport where id = ?";
	
	private static final String SELECT_QUERY_IMPORT_FAILED_RECORDS = "select failedrowcount from ingest_dataimport where id = ?";
	
	private static final String UPDATE_COUNT_QUERY_IMPORT = "update ingest_dataimport set failedrowcount =  ifnull(failedrowcount,0) + ? , updated_date = ?,"
			+ "    error =  ?, import_status = ?  where id  = ?";
	
	private static final String UPDATE_COUNT_QUERY_IMPORT_H2 = "update ingest_dataimport set  failedrowcount =  ? , updated_date = ?,"
			+ "    error = ?, import_status = ?  where id  = ?";

	@Qualifier("jdbcEnrichmentDBNamedTemplate")
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@PostConstruct
	public void init() {
	}

	@Value("${h2.db:false}")
	private boolean isH2;
	
	public void updateDataImportStatus(int id, ImportStatus importStatus, final String errorMessage)
			throws Exception {
		int status = jdbcTemplate.update(UPDATE_STATUS_QUERY_IMPORT, importStatus.toString(), new Date(), errorMessage,
				id);
		if (status == 0) {
			throw new Exception("unable to update import status for ID: " + id);
		}
	}

	public String getDataImportStatus(int id) throws Exception {
		return this.jdbcTemplate.queryForObject(SELECT_QUERY_IMPORT_STATUS, String.class, id);
	}
	
	public long getDataImportFailedRecords(int id) throws Exception {
		return this.jdbcTemplate.queryForObject(SELECT_QUERY_IMPORT_FAILED_RECORDS, Long.class, id);
	}
	
	public void updateDataImportRecords(long id, long failedRecords, final String errorMessage) throws Exception {
		int status = 0;
		if (isH2) {
			status = jdbcTemplate.update(UPDATE_COUNT_QUERY_IMPORT_H2,  failedRecords, new Date(), errorMessage, ImportStatus.INGEST_FAILED.toString(), id);
		} else {
			status = jdbcTemplate.update(UPDATE_COUNT_QUERY_IMPORT,  failedRecords, new Date(), errorMessage, ImportStatus.INGEST_FAILED.toString(), id);
		}

		if (status == 0)
			throw new Exception("unable to update import records for ID: " + id);

	}
}
