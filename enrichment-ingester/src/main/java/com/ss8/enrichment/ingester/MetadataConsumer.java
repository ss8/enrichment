package com.ss8.enrichment.ingester;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.common.geo.GeoPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import com.ss8.enrichment.ingester.config.ElasticSearchConfiguration;
import com.ss8.enrichment.ingester.dao.ImportStatusUpdateDao;
import com.ss8.enrichment.ingester.service.CacheService;
import com.ss8.enrichment.ingester.util.EnrichmentUtil;
import com.ss8.iql.query.ElasticQueryWrapper;
import com.ss8.metahub.shared.dto.EnrichmentDatasourceType;
import com.ss8.metahub.shared.dto.IngestStatistics;
import com.ss8.metahub.shared.dto.Metadata;
import com.ss8.metahub.shared.dto.MetadataWrapper;
import com.ss8.metahub.shared.util.GlobalConstants;
import com.ss8.shared.dao.SharedImportDao;

public class MetadataConsumer {

	private static final Logger LOGGER = LoggerFactory.getLogger(MetadataConsumer.class);

	@Value("${enrichment.elastic.writeIndexName:enrichment-index}")
	private String writeIndexName;
	
	@Value("${enrichment.elastic.readIndexName:enrichment}")
	private String readIndexName;
	
	@Value("${enrichment.elastic.batchSize:2000}")
	private int batchSize;

	@Value("${kafka.enrichment.topic}")
	private String enrichmentTopic;

	@Value("${enrichment.ingester.failedRecords:true}")
	private boolean checkForElasticFailedRecords;

	@Value("${enrichment.ingester.profileingester:false}")
	private boolean profileIngester;

	@Value("${elastic.indexing.timeout:10}")
	private int elasticIndexingTimeout;

	@Value("${elastic.writeRetires:3}")
	private int elasticRetires;

	@Autowired
	ImportStatusUpdateDao dao;

	private ConcurrentHashMap<String, IngestStatistics> ingestStatsMap = new ConcurrentHashMap<String, IngestStatistics>();

	private static String ACTIVE = "active";
	
	@Value("${ixng.gr.site:SITEA}")
    private String siteName;
	
    @Autowired
    private CacheService cacheService;
    
	@Autowired
	private SharedImportDao importDao;
    
	@Autowired
	private ElasticSearchConfiguration elasticConfig;

	public MetadataConsumer() {}

	@KafkaListener(topics = "#{'${kafka.enrichment.topic}'.split(',')}", groupId = "${spring.kafka.consumer.enrichmentConsumer-group-id}")
	public void receive(@Payload List<MetadataWrapper> metadataList,
			final Acknowledgment acknowledgment,
			@Header(KafkaHeaders.RECEIVED_PARTITION_ID) List<Integer> partitions,
			@Header(KafkaHeaders.GROUP_ID) String groupId)
			throws InterruptedException {
		long startConsumer = System.nanoTime();
		String uuid = UUID.randomUUID().toString();
		try {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Enrichment Records received with size - "
						+ metadataList.size() + " for topic -> " + enrichmentTopic
						+ " and partition id - " + partitions.get(0));
			}
			
			ElasticQueryWrapper elasticWrapper = elasticConfig.getElasticInstance();
			
			boolean isPassiveSite;
			LOGGER.debug("SiteName =" + siteName + ", SiteStatus = "
					+ cacheService.getCurrentSiteStatus(siteName));
			if (null != cacheService.getCurrentSiteStatus(siteName)
					&& cacheService.getCurrentSiteStatus(siteName)
							.equals(ACTIVE)) {
				isPassiveSite = false;
			} else {
				isPassiveSite = true;
			}

			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("SiteName = {}, isPassiveSite = {}", siteName,
						isPassiveSite);
			}
            
			List<MetadataWrapper> importsToDelete = new ArrayList<>();
			if (isPassiveSite) {
				for (MetadataWrapper metadataWrapper : metadataList) {
					String operationType = metadataWrapper.getOperationType();
					if (MetadataWrapper.TYPE_CREATE.equals(operationType)) {
						// skip - will handle in insert part
					} else if (MetadataWrapper.TYPE_DELETE_IMPORT
							.equals(operationType)) {
						importsToDelete.add(metadataWrapper);
					} else {
						LOGGER.warn("unsupported operation type: {}",
								operationType);
					}
				}
			}

			HashMap<Long, List<IndexRequest>> importIdToMetadataListMapForElastic = createImportIdToMetadataListElastic(
					metadataList, isPassiveSite);
			logProfileRecord("Total time for creating data to Elastic-->"
					+ ((System.nanoTime() - startConsumer) / 1000000) + " ms " + "--" + uuid);
			
			for (long importId : importIdToMetadataListMapForElastic.keySet()) {
				try {
					long startTime = System.nanoTime();
					long startTimeElastic = System.nanoTime();
					int elastic_batch_retry = 0;
					long failedDocs = 0;
					while (elastic_batch_retry <= elasticRetires) {
						failedDocs = 0; // assign 0 so that every iteration we don't need add the failed docs
						try {
							BulkResponse response = elasticWrapper.insertDocsToElastic(
									importIdToMetadataListMapForElastic.get(importId), elasticIndexingTimeout);
							if (checkForElasticFailedRecords) {
								for (BulkItemResponse docResponse : response.getItems()) {
									if (null == docResponse.getResponse()) { // failure
										failedDocs++;
										LOGGER.error("errors when inserting docs for import (" + importId + ") "
												+ docResponse.getFailureMessage() + "--retry--- "
												+ (elastic_batch_retry));
									}
								}
							}
							if (failedDocs == 0) {
							break; // no errors break it
							}
						} catch (SocketTimeoutException ste) {
							LOGGER.error("errors when inserting docs for import (" + importId + ") with retry  ("
									+ elastic_batch_retry + ")", ste);
							elastic_batch_retry++;
						} catch (Exception e) { // other exception just throw back
							throw e;
						}
					}
					if (failedDocs > 0 && !isPassiveSite) {
						dao.updateDataImportRecords(importId, failedDocs,
								" Error while inserting to elastic check logs ");
					}

					logProfileRecord("Total time for ingesting  data to Elastic-->"
							+ ((System.nanoTime() - startTimeElastic) / 1000000) + " ms for docs-->"
							+ importIdToMetadataListMapForElastic.get(importId).size() + "--" + uuid);
					logProfileRecord("Total time for ingesting  data to Elastic-->"
							+ ((System.nanoTime() - startTime) / 1000000) + " ms for docs-->"
							+ importIdToMetadataListMapForElastic.get(importId).size() + "--" + uuid);
				} catch (Exception e) {
					LOGGER.error("errors when inserting docs for import (" + importId + ") ", e);
					// If there is any exception while inserting, update the respective import
					// record's status to failed.
					if (!isPassiveSite) {
						dao.updateDataImportRecords(importId,
								importIdToMetadataListMapForElastic
										.get(importId).size(),
								" Error while inserting data check logs ");
					}
				}
			}
			
			if (!importsToDelete.isEmpty()) {
				for (MetadataWrapper metadataWrapper : importsToDelete) {
					if (!MetadataWrapper.TYPE_DELETE_IMPORT
							.equals(metadataWrapper.getOperationType())) {
						continue;
					}

					long datasourceId = metadataWrapper.getDatasourceId();
					long importId = metadataWrapper.getImportId();

					boolean deleteSuccess = importDao
							.deleteByImportAndDatasourceId(importId,
									datasourceId, readIndexName);
					if (!deleteSuccess) {
						LOGGER.error(
								"failed deleting elasitc documents for imortId - "
										+ importId + ", datasourceId - "
										+ datasourceId);
					}
				}
			}			
		} catch (Exception e) {
			LOGGER.error("error while getting data-->", e);
		} finally {
			acknowledgment.acknowledge();
			long endTime = ((System.nanoTime() - startConsumer) / 1000000);
			IngestStatistics ingestStats = ingestStatsMap.get(Thread.currentThread().getName());
			if (ingestStats == null) {
				ingestStats = new IngestStatistics();
				ingestStatsMap.put(Thread.currentThread().getName(), ingestStats);
			}
			ingestStats.setTotalIngestedDocsCount((ingestStats.getTotalIngestedDocsCount() + metadataList.size()));
			ingestStats.setTotalTimeTaken((ingestStats.getTotalTimeTaken() + endTime));
			ingestStats.setTotalIterations((ingestStats.getTotalIterations() + 1));
			ingestStats.setAverageBatchSize(((ingestStats.getTotalIngestedDocsCount() + metadataList.size())
					/ ingestStats.getTotalIterations()));
			ingestStats.setAverageTimeTaken((ingestStats.getTotalTimeTaken() / ingestStats.getTotalIterations()));
			LOGGER.info("Finished processing the enrichment Records received with size - " + metadataList.size()
					+ " for topic -> " + enrichmentTopic + " and partition id - " + partitions.get(0)
					+ "--time taken -->" + endTime + "ms -- Total docs ingested so far is -->"
					+ ingestStats.getTotalIngestedDocsCount() + " with average batch size --> "
					+ ingestStats.getAverageBatchSize() + " along with "
					+ ((ingestStats.getAverageBatchSize() / (ingestStats.getAverageTimeTaken() / 1000)))
					+ " docs/sec and total average time taken so far --> " + ingestStats.getAverageTimeTaken()
					+ "ms -- " + uuid);
		}
	}

	private void logProfileRecord(String msg) {
		if (profileIngester) {
			LOGGER.info(msg);
		} else {
			LOGGER.debug(msg);
		}
	}

	private HashMap<Long, List<IndexRequest>> createImportIdToMetadataListElastic(
			final List<MetadataWrapper> metadataList, boolean isPassiveSite)
			throws Exception {
		HashMap<Long, List<IndexRequest>> importIdToMetadataListMap = new HashMap<Long, List<IndexRequest>>();
		HashMap<Long, Integer> failedCountImportIdMap = new HashMap<Long, Integer>();
		for (MetadataWrapper metadataWrapper : metadataList) {
			if (!MetadataWrapper.TYPE_CREATE
					.equals(metadataWrapper.getOperationType())) {
				continue;
			}
			
			Metadata metadata = metadataWrapper.getMetadata();
			IndexRequest indexRequest = new IndexRequest(this.writeIndexName);
			if (importIdToMetadataListMap.get(metadata.getImportId()) == null) {
				importIdToMetadataListMap.put(metadata.getImportId(), new LinkedList<IndexRequest>());
			}
			Map<String, Object> doc = new HashMap<>();
			doc.put(GlobalConstants.FIELD_EVENT_TYPE, metadata.getEventType());
			doc.put(GlobalConstants.FIELD_DATA_SOURCE_ID, metadata.getDatasourceId());
			doc.put(GlobalConstants.FIELD_DATA_SOURCE_TYPE, metadata.getDatasourceType());
			doc.put(GlobalConstants.FIELD_DATA_SOURCE_NAME, metadata.getDatasourceName());
			doc.put(GlobalConstants.FIELD_IMPORT_ID, metadata.getImportId());
			doc.put(GlobalConstants.FIELD_IMPORT_NAME, metadata.getImportName());
			if (metadata.getMetadataTextMap() != null) {
				for (String field : metadata.getMetadataTextMap().keySet()) {
					doc.put(field, metadata.getMetadataTextMap().get(field));
				}
			}
			if (metadata.getMetadataDoubleMap() != null) {
				if (EnrichmentDatasourceType.BSA.name().equals(metadata.getDatasourceType()) 
						&& metadata.getMetadataDoubleMap().get(GlobalConstants.AZIMUTH) != null
						&& metadata.getMetadataDoubleMap().get(GlobalConstants.AZIMUTH) > 360.0) {
					if (failedCountImportIdMap.get(metadata.getImportId()) == null) {
						failedCountImportIdMap.put(metadata.getImportId(), 0);
					}
					failedCountImportIdMap.put(metadata.getImportId(), failedCountImportIdMap.get(metadata.getImportId()) + 1);
					LOGGER.error("Azimuth value is --> " + metadata.getMetadataDoubleMap().get(GlobalConstants.AZIMUTH) 
							+ " which is greater than 360 for the import id --> " + metadata.getImportId() + ", hence skipping this record");
					continue;
				}
				for (String field : metadata.getMetadataDoubleMap().keySet()) {
					doc.put(field, metadata.getMetadataDoubleMap().get(field));
				}
			}
			if (metadata.getMetadatatimestampMap() != null) {
				for (String field : metadata.getMetadatatimestampMap().keySet()) {
					doc.put(field, metadata.getMetadatatimestampMap().get(field));
				}
			}
			if (metadata.getMetadataBooleanMap() != null) {
				for (String field : metadata.getMetadataBooleanMap().keySet()) {
					doc.put(field, metadata.getMetadataBooleanMap().get(field));
				}
			}
			if (metadata.getEventLocations() != null && metadata.getEventLocations().size() > 0) {
				List<GeoPoint> geoPoints = new LinkedList<GeoPoint>();
				for (com.ss8.metahub.shared.dto.GeoPoint geoPoint : metadata.getEventLocations()) {
					geoPoints.add(new GeoPoint(geoPoint.getLatitude(), geoPoint.getLongitude()));
				}
				// Add Lat, Long and Geometry to the elastic document.
				if (null != geoPoints) {
					List<Double> latLong = Arrays
							.asList(new Double[] { geoPoints.get(0).getLon(), geoPoints.get(0).getLat() });
					doc.put(GlobalConstants.FIELD_EVENT_LOCATIONS, latLong);
					if (EnrichmentDatasourceType.BSA.name().equals(metadata.getDatasourceType())) {
						Map<String, Object> geoShapeMap = EnrichmentUtil.getGeometryForElastic(geoPoints, metadata);
						doc.put(GlobalConstants.FIELD_GEOMETRY, geoShapeMap);
					}
				}
			}
			if (metadata.getMetadataLocationMap() != null) {
				for (String field : metadata.getMetadataLocationMap().keySet()) {
					List<GeoPoint> geoPoints = new LinkedList<GeoPoint>();
					for (com.ss8.metahub.shared.dto.GeoPoint geoPoint : metadata.getMetadataLocationMap().get(field)) {
						geoPoints.add(new GeoPoint(geoPoint.getLatitude(), geoPoint.getLongitude()));
					}
					if (geoPoints.size() > 0) {
						doc.put(field, geoPoints);
					}
				}
			}
			indexRequest.id(metadata.getId());
			indexRequest.source(doc);
			importIdToMetadataListMap.get(metadata.getImportId()).add(indexRequest);
		}
		
		if (!isPassiveSite) {
			if (failedCountImportIdMap.size() > 0) {
				for (Long importId : failedCountImportIdMap.keySet()) {
					dao.updateDataImportRecords(importId,
							failedCountImportIdMap.get(importId),
							" Some records were skipped, please check logs ");
				}
			}
		}
		
		return importIdToMetadataListMap;
	}
	
}