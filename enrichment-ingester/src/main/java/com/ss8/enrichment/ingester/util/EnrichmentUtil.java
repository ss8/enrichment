package com.ss8.enrichment.ingester.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.elasticsearch.common.geo.GeoPoint;

import com.ss8.metahub.shared.dto.Metadata;
import com.ss8.metahub.shared.util.GlobalConstants;
import com.ss8.metahub.shared.util.PolygonCoordinates;

public class EnrichmentUtil {
	public static Map<String, Object> getGeometryForElastic(List<GeoPoint> geoPoints, Metadata metadata) {
		List<double[][]> polyList = new ArrayList<>();
		List<com.ss8.metahub.shared.dto.GeoPoint> geoPointList = new ArrayList<>();
		
		Double azimuth = 0.00;
		Double width = 0.00;
		Double range = 0.00;
		
		if (metadata.getMetadataDoubleMap() != null) {
			for (String field : metadata.getMetadataDoubleMap().keySet()) {
				if (GlobalConstants.AZIMUTH.equals(field)) {
					azimuth = metadata.getMetadataDoubleMap().get(field);
				} else if (GlobalConstants.RANGE.equals(field)) {
					range = metadata.getMetadataDoubleMap().get(field);
				} else if (GlobalConstants.WIDTH.equals(field)) {
					width = metadata.getMetadataDoubleMap().get(field);
				}
			}
		}

		if (null == azimuth || Double.compare(width, 360) == 0) {
			azimuth = 0.00;
			width = 360.00;
		} else if (Double.compare(azimuth, 0.00) > 0 && Double.compare(azimuth, 120) <= 0) {
			azimuth = 60.00;
			width = 120.00;
		} else if (Double.compare(azimuth, 121.00) > 0 && Double.compare(azimuth, 240) <= 0) {
			azimuth = 180.00;
			width = 120.00;
		} else if (Double.compare(azimuth, 241.00) > 0 && Double.compare(azimuth, 360) <= 0) {
			azimuth = 240.00;
			width = 120.00;
		}
		geoPointList = PolygonCoordinates.createPolygon(geoPoints.get(0).getLon(), geoPoints.get(0).getLat(), azimuth,
				width, range);
		double[][] coords = new double[geoPointList.size()][2];
		for (int i = 0; i < geoPointList.size(); i++) {
			for (int j = 0; j < 2; j++) {
				coords[i][0] = geoPointList.get(i).getLongitude();
				coords[i][1] = geoPointList.get(i).getLatitude();
			}
		}
		polyList.add(coords);
		Map<String, Object> geoShapeMap = new HashMap<>();
		geoShapeMap.put(GlobalConstants.GEOMETRY_TYPE, GlobalConstants.GEOMETRY_POLYGON);
		geoShapeMap.put(GlobalConstants.GEOMETRY_COORDINATES, polyList);
		return geoShapeMap;

	}
}
