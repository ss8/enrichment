package com.ss8.enrichment.enrichmentservice.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PopFeature {
	
	@SerializedName("id")
	@Expose
	public String id;
	@SerializedName("type")
	@Expose
	public String type;
	@SerializedName("geometry")
	@Expose
	public Geometry geometry;
	@SerializedName("properties")
	@Expose
	public PopProperties properties;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Geometry getGeometry() {
		return geometry;
	}
	public void setGeometry(Geometry geometry) {
		this.geometry = geometry;
	}
	public PopProperties getProperties() {
		return properties;
	}
	public void setProperties(PopProperties properties) {
		this.properties = properties;
	}
	
}
