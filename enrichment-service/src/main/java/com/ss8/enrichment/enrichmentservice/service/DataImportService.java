package com.ss8.enrichment.enrichmentservice.service;

import java.io.IOException;

import com.ss8.metahub.shared.dto.ImportStatusDTO;
import com.ss8.metahub.shared.dto.IngestDataImportDTO;
import com.ss8.metahub.shared.dto.IngestDataSourceDTO;

public interface DataImportService {
	
	public boolean processDataStreamTask(IngestDataSourceDTO dto) throws IOException, Exception;
	
	public void processDataImportTask(IngestDataImportDTO dto, IngestDataSourceDTO datasourceDto) throws IOException, Exception;
	
	public void processDataStreamDelete(IngestDataSourceDTO dto) throws IOException, Exception;
	
	public IngestDataSourceDTO addDataSource(IngestDataSourceDTO dto) throws Exception;

	public void scheduleDataImportDelete(int id) throws Exception;
	
	public void updateDataImportStatus(final long id, final ImportStatusDTO importStatusDTO) throws Exception;
}
