package com.ss8.enrichment.enrichmentservice.service.impl;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeSet;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.PutMappingRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ss8.enrichment.enrichmentservice.config.ElasticSearchConfiguration;
import com.ss8.enrichment.enrichmentservice.service.DataImportService;
import com.ss8.enrichment.enrichmentservice.service.QueryConfigService;
import com.ss8.iql.query.dto.SourceType;
import com.ss8.metahub.shared.dto.DatasourceType;
import com.ss8.metahub.shared.dto.FieldDisplayName;
import com.ss8.metahub.shared.dto.ImportEventDto;
import com.ss8.metahub.shared.dto.ImportField;
import com.ss8.metahub.shared.dto.ImportRecord;
import com.ss8.metahub.shared.dto.ImportStatus;
import com.ss8.metahub.shared.dto.ImportStatusDTO;
import com.ss8.metahub.shared.dto.IngestDataImportDTO;
import com.ss8.metahub.shared.dto.IngestDataSourceDTO;
import com.ss8.metahub.shared.dto.MetadataWrapper;
import com.ss8.metahub.shared.util.FilesUtil;
import com.ss8.metahub.shared.util.GlobalConstants;
import com.ss8.metahub.shared.util.ImportUtility;
import com.ss8.shared.dao.IngestDataSourceDAO;
import com.ss8.shared.dao.SharedImportDao;

@Service
@MessageEndpoint
public class DataImportServiceImpl implements DataImportService {

	private static final Logger logger = LoggerFactory.getLogger(DataImportServiceImpl.class);
	private static final String ENRICHMENT_STREAM_PREFIX = "EnrichmentStream_";
	
	@Autowired
	private QueryConfigService queryConfigService;

	@Autowired
	ApplicationEventPublisher applicationEventPublisher;

	@Autowired
	IngestDataSourceDAO dao;

	@Autowired
	SharedImportDao importDao;
	
	@Value("${kafka.fileparser.topic:FILE-PARSER-TOPIC}")
	private String fileParserTopic;
	
	@Autowired
	@Qualifier("fileParserKafkaTemplate")
	private KafkaTemplate<String, ImportRecord> kafkaTemplate;
	
	@Value("${kafka.enrichment.producer.topic:ENRICHMENT-TOPIC}")
	private String enrichmentTopic;

	@Autowired
	@Qualifier("enrichmentKafkaTemplate")
	private KafkaTemplate<String, MetadataWrapper> enrichmentKafkaTemplate;

	private RestHighLevelClient restHighLevelClient;
	
	@Value("${enrichment.elastic.readIndexName:enrichment}")
	private String readIndexName;
	
	@Value("${enrichment.elastic.writeIndexName:enrichment-index}")
	private String writeIndexName;
	
	@Value("${enrichment.filewatcher.corepath:/var/enrichment/upload}")
	private String watcherPath;

	@Value("${enrichment.elastic.refeshrate.seconds:2}")
	private long elasticRefreshRate;

	@Value("${enrichment.dataimportdelete.scheduled.wait.seconds:300}")
	private long scheduledWaitTime;
	
	@Value("${enrichment.dataimportdelete.importDataBatchSize:2000}")
	private int deleteBatchSize;

	@Value("${kafka.streaming.topic.partitions:12}")
	private int streamingTopicPartitions;
	
	@Value("${kafka.streaming.topic.replicas:1}")
	private int streamingTopicReplicas;
	
	@Autowired
	private AdminClient admin;
	
	@Value("${inxt.gr.enabled:false}")
	private boolean grEnabled;
	
	@Autowired
	private ElasticSearchConfiguration elasticConfig;

	public DataImportServiceImpl(@Qualifier("elasticSearchConfiguration")RestHighLevelClient restHighLevelClient) {
		this.restHighLevelClient = restHighLevelClient;
	}
	
	@Override
	public void processDataStreamDelete(IngestDataSourceDTO dto) throws Exception {
		//deleting topic
		logger.info("######################### Deleting topic - " + ENRICHMENT_STREAM_PREFIX + dto.getName());
		admin.deleteTopics(Arrays.asList(new String[]{ENRICHMENT_STREAM_PREFIX + dto.getName()}));
	}
	
	
	/*
	 * Sets the details like Display Name, Derived field, Mapped Field, 
	 * Fields used for merging etc. on the columns defined for creating the datasource
	 */
	private void setColumnsToTypeMap(ImportRecord importRecord,IngestDataSourceDTO dto) {
		String jsonColumnsToMap = dto.getImportColumnsToTypeMap();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode rootNode = null;
		try {
			rootNode = mapper.readTree(jsonColumnsToMap);
		} catch (JsonMappingException e) {
			logger.error("JSON Mapping Exception"+ e.getMessage());
		} catch (JsonProcessingException e) {
			logger.error("JSON Processing Exception"+ e.getMessage());
		}
		Iterator<Map.Entry<String, JsonNode>> fields = rootNode.fields();
		while (fields.hasNext()) {
			Map.Entry<String, JsonNode> entry = fields.next();
			JsonNode node = entry.getValue();
			ImportField importField = new ImportField();
			if (null != node.get("dataType"))
				importField.setDataType(node.get("dataType").textValue());
			if (null != node.get("dateTimeFormat"))
				importField.setDateTimeFormat(node.get("dateTimeFormat").textValue());
			importField.setFieldName(entry.getKey());
			if (null != node.get("mappedField"))
				importField.setMappedField(node.get("mappedField").textValue());
			if (null != node.get("displayName"))
				importField.setDisplayName(node.get("displayName").textValue());
			if (node.get("fieldsToMerge") != null && node.get("fieldsToMerge").isArray()) {
				LinkedList<String> fieldsToMerge = new LinkedList<String>();
				for (final JsonNode fieldToMerge : node.get("fieldsToMerge")) {
					fieldsToMerge.add(fieldToMerge.textValue());
			    }
				importField.setFieldsToMerge(fieldsToMerge);
			}
			if (null != node.get("derivedField")) {
				importField.setDerivedField(node.get("derivedField").textValue());
			}
			importRecord.setImportColumnsToTypeMap(entry.getKey(), importField);
		}
	}
	
	@Override
	public boolean processDataStreamTask(final IngestDataSourceDTO dto) throws Exception {
		try {		
			ImportRecord importRecord = new ImportRecord();
			// Get the actual type of the event name.
			importRecord.setEventType(dto.getEventType());
			importRecord.setDataSourceId(dto.getId());
			importRecord.setDataSourceName(dto.getName());
			importRecord.setDatasourceData(dto);
			importRecord.setUsStyleDateFormat(dto.isUsStyleDateFormat());
			setColumnsToTypeMap(importRecord, dto);
			// Find all the available fields from query config table.
			TreeSet<String> fieldsSet = importDao.getQueryFieldsByCollection(readIndexName);
			boolean isElasticSchemaUpdated = false;
			for (String field : importRecord.getImportColumnsToTypeMap().keySet()) {
				ImportField importField = importRecord.getImportColumnsToTypeMap().get(field);
				// If the mapped field name is not present, then only add that to elastic and update the query config table.
				if (!fieldsSet.contains(importField.getMappedField())) {
					if (this.addFieldToElastic(importField.getMappedField(), importField.getDisplayName(), importField.getDataType())) {
						isElasticSchemaUpdated = true;
					} else {
						String errorMessage = "Streaming unsuccessful because the insertion of mapped field - " 
								+ importField.getMappedField() + " to elastic search failed.";
						// Log the import error message.
						logger.error("DataImportServiceImpl :: processDataStreamTask() :: " + errorMessage);
						dao.deleteDataSource(dto.getId());
						return false;
					}
				}
			}
			if (isElasticSchemaUpdated) {
				// creating new topic
				logger.info("######################### Creating topic - " + ENRICHMENT_STREAM_PREFIX + dto.getName());
				NewTopic newTopic = new NewTopic(ENRICHMENT_STREAM_PREFIX + dto.getName(), streamingTopicPartitions,
						(short) streamingTopicReplicas);
				admin.createTopics(Collections.singleton(newTopic));
				applicationEventPublisher.publishEvent(new ImportEventDto());
			}
		} catch (Exception e) {
			logger.error(
				"DataImportServiceImpl :: processDataStreamTask() :: Stream exception - " + e.getMessage());
			dao.deleteDataSource(dto.getId());
			return false;
		}
		return true;
	}
	
	@Override
	public void processDataImportTask(IngestDataImportDTO dto, IngestDataSourceDTO datasourceDto) throws Exception {
		logger.info("#########################Data Import:" + dto.toString());
		try {
			ImportRecord importRecord = new ImportRecord();
			// Get the actual type of the event name.
			importRecord.setEventType(datasourceDto.getEventType());
			importRecord.setDataSourceId(dto.getDatasource());
			importRecord.setDataSourceName(datasourceDto.getName());
			importRecord.setImportId(dto.getId());
			importRecord.setImportName(dto.getName());
			importRecord.setUsStyleDateFormat(datasourceDto.isUsStyleDateFormat());
			setColumnsToTypeMap(importRecord, datasourceDto);
			importRecord.setFilePath(dto.getFilename());
			importRecord.setFileType(datasourceDto.getFileType());
			// Find all the available fields from query config table.
			TreeSet<String> fieldsSet = importDao.getQueryFieldsByCollection(readIndexName);
			boolean isElasticSchemaUpdated = false;
			for (String field : importRecord.getImportColumnsToTypeMap().keySet()) {
				ImportField importField = importRecord.getImportColumnsToTypeMap().get(field);
				// If the mapped field name is not present, then only add that to elastic and update the query config table.
				if (!fieldsSet.contains(importField.getMappedField())) {
					if (this.addFieldToElastic(importField.getMappedField(), importField.getDisplayName(), importField.getDataType())) {
						isElasticSchemaUpdated = true;
					} else {
						String errorMessage = "Import unsuccessful because the insertion of mapped field - " 
								+ importField.getMappedField() + " to elastic search failed.";
						// Log the import error message.
						logger.error("DataImportServiceImpl :: processDataImportTask() :: " + errorMessage);
						dao.updateFailedRecordsCount(dto.getId(), 0, ImportStatus.INGEST_FAILED, "Import exception, please refer to the logs for more details.");
						dto.setStatus(ImportStatus.INGEST_FAILED.toString());
						return;
					}
				}
			}
			importRecord.setDatasourceType(DatasourceType.ENRICHMENT_DATA_SOURCE.toString());
			importRecord.setDatasourceSubType(datasourceDto.getDatasourceType());
			// Send the import record to file parser topic for further processing
			kafkaTemplate.send(fileParserTopic, importRecord);
			// Check whether the elastic schema was updated, if so publish the import event.
			if (isElasticSchemaUpdated) {
				applicationEventPublisher.publishEvent(new ImportEventDto());
			}
			
		} catch (Exception e) {
			// Log the import exception message.
			logger.error(
					"DataImportServiceImpl :: processDataImportTask() :: Import exception - " + e.getMessage());
			dao.updateFailedRecordsCount(dto.getId(), 0, ImportStatus.INGEST_FAILED, "Import exception, please refer to the logs for more details.");
			dto.setStatus(ImportStatus.INGEST_FAILED.toString());
		}
	}

	@Override
	public IngestDataSourceDTO addDataSource(IngestDataSourceDTO dto) throws Exception {
		// Create the respective mapping for all the fields.
		LinkedHashMap<String, ImportField> importColumnsToTypeMap = ImportUtility.getImportColumnsToTypeMap(dto.getImportColumnsToTypeMap());
		// Get all the display name to fields mapping from the csvFieldNamesWithTypeMapping.
		Map<String, SourceType> sourcesTypesMap = queryConfigService.getQueryConfig().getSourceTypes().getSourceTypesMap();
		SourceType sourceType = sourcesTypesMap.get("fusion");
		ArrayList<String> sources = sourceType.getEventTypes().getEventTypesMap().get("all").getSourceNames();
		List<FieldDisplayName> currentfields = importDao.getFieldDisplayNameMapping(sources);
		ImportUtility.updateMappedField(importColumnsToTypeMap, currentfields);
		ObjectMapper mapper = new ObjectMapper();
		dto.setImportColumnsToTypeMap(mapper.writeValueAsString(importColumnsToTypeMap));
		if (dto.getDatasourceFolderName() == null) {
			dto.setDatasourceFolderName(watcherPath + "/" + dto.getName());
		} else {
			dto.setDatasourceFolderName(watcherPath + "/" + dto.getDatasourceFolderName());
		}
		// Create the data source directory with the folder name by prefixing the parent directory path.
		FilesUtil.getInstance().createDirectoriesWithDefaultAttributes(
				Paths.get(dto.getDatasourceFolderName()));
		return dao.addDataSource(dto);
	}
	
	private boolean addFieldToElastic(String fieldName, String displayName, String dataType) throws IOException {
		FieldDisplayName field = new FieldDisplayName();
		PutMappingRequest request = new PutMappingRequest(writeIndexName);
		Map<String, Object> properties = new HashMap<>();
		Map<String, Object> typeId = new HashMap<>();
		Map<String, Object> jsonMap = new HashMap<>();
		if (GlobalConstants.DATA_TYPE_DATE.equals(dataType)) {
			typeId.put("type", "date");
			field.setDataType(GlobalConstants.DATA_TYPE_DATE);
		} else if (GlobalConstants.DATA_TYPE_NUMBER.equals(dataType)) {
			typeId.put("type", "double");
			field.setDataType(GlobalConstants.DATA_TYPE_NUMBER);
		} else if (GlobalConstants.DATA_TYPE_BOOLEAN.equals(dataType)) {
			typeId.put("type", "boolean");
			field.setDataType(GlobalConstants.DATA_TYPE_BOOLEAN);
		} else if (GlobalConstants.DATA_TYPE_GEO_POINT.equals(dataType)) {
			typeId.put("type", "geo_point");
			field.setDataType(GlobalConstants.DATA_TYPE_GEO_POINT);
		} else if (GlobalConstants.DATA_TYPE_CELLID.equals(dataType)) {
			typeId.put("type", "keyword");
			typeId.put("copy_to", GlobalConstants.FIELD_GLOBAL_SEARCH_TEXT);
			field.setDataType(GlobalConstants.DATA_TYPE_CELLID);
		} else {
			typeId.put("type", "keyword");
			typeId.put("copy_to", GlobalConstants.FIELD_GLOBAL_SEARCH_TEXT);
			field.setDataType(GlobalConstants.DATA_TYPE_TEXT);
		}
		properties.put(fieldName, typeId);
		jsonMap.put("properties", properties);
		System.out.println("prop@@-->" + jsonMap);
		request.source(jsonMap);
		org.elasticsearch.action.support.master.AcknowledgedResponse acknowledgedResponse = restHighLevelClient.indices()
				.putMapping(request, RequestOptions.DEFAULT);
		if (acknowledgedResponse.isAcknowledged()) {
			// add data to database
			field.setField(fieldName);
			if (null != displayName) {
				field.setDisplayName(displayName);
			} else {
				field.setDisplayName(fieldName);
			}
			field.setCollection(readIndexName);
			importDao.updateImportFieldDisplayNameMappings(field);
			return true;
		}
		return false;
	}

	@Override
	public void scheduleDataImportDelete(int id) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("scheduleDataImportDelete: id - " + id);
		}
		
		Timer importDeleteTimer = new Timer();
		TimerTask deleteTask = new TimerTask() {			
			@Override
			public void run() {
				try {
					List<IngestDataImportDTO> list = dao.getDataImportbyId(id);
					if (importDao.deleteByImportAndDatasourceId(list.get(0).getId(), list.get(0).getDatasource(), readIndexName)) {
						dao.deleteDataImport(id);
						
						if (grEnabled) {
							MetadataWrapper metadataWrapper = new MetadataWrapper();
							long datasourceId = list.get(0).getDatasource();
							metadataWrapper.setImportId(id);
							metadataWrapper.setDatasourceId(datasourceId);
							
							metadataWrapper.setOperationType(
									MetadataWrapper.TYPE_DELETE_IMPORT);

							enrichmentKafkaTemplate.send(enrichmentTopic,
									metadataWrapper);
						}
						
						logger.info("Publishing import event to notify the import event listener to refresh query config cache");
						applicationEventPublisher.publishEvent(new ImportEventDto());
					} else {
						logger.error("Unable to delete dataimport " + id);
					}
				} catch (Exception e) {
					logger.error("Unable to delete dataimport " + id, e);
				}
			}
		};
		
		importDeleteTimer.schedule(deleteTask, (elasticRefreshRate + scheduledWaitTime) * 1000);
	}

	@Override
	public void updateDataImportStatus(final long id, final ImportStatusDTO importStatusDTO) throws Exception {
		dao.updateDataImportStatus(id, importStatusDTO);
	}
}
