package com.ss8.enrichment.enrichmentservice.service;

import java.io.IOException;

import com.ss8.iql.query.dto.QueryBuilderConfig;


public interface QueryConfigService {
	
	public QueryBuilderConfig getQueryConfig() throws IOException;
	
	public void flushAndFetchQueryConfig() throws IOException;
}
