package com.ss8.enrichment.enrichmentservice.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import com.ss8.metahub.shared.dto.ImportRecord;
import com.ss8.metahub.shared.dto.MetadataWrapper;

@Configuration
public class MetadataProducerConfig {

	@Value("${spring.kafka.bootstrap-servers}")
	private String bootstrapServers;
	
	@Value("${spring.kafka.producer.retries}")
	private int retries;
	
	@Value("${spring.kafka.producer.batch-size}")
	private int batchSize;
	
	@Value("${spring.kafka.producer.linger-ms}")
	private long lingerInMs;
	
	@Value("${spring.kafka.producer.buffer-memory}")
	private long bufferMemory;
	
	@Value("${kafka.fileparser.topic}")
	private String fileParserTopic;
	
	@Value("${kafka.fileparser.topic.partitions:12}")
	private int fileParserTopicPartitions;
	
	@Value("${kafka.fileparser.topic.replicas:1}")
	private int fileParserTopicReplicas;

	@Value("${kafka.enrichment.producer.topic:ENRICHMENT-TOPIC}")
	private String enrichmentTopic;
	
	@Value("${kafka.enrichment.producer.topic.partitions:10}")
	private int enrichmentTopicPartitions;
	
	@Value("${kafka.enrichment.producer.topic.replicas:1}")
	private int enrichmentTopicReplicas;
	
	@Bean
	public Map<String, Object> metadataProducerConfigs() {
		Map<String, Object> props = new HashMap<>();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		props.put(ProducerConfig.RETRIES_CONFIG, retries);
		props.put(ProducerConfig.BATCH_SIZE_CONFIG, batchSize);
		props.put(ProducerConfig.LINGER_MS_CONFIG, lingerInMs);
		props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, bufferMemory);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
		return props;
	}
	
	@Bean
	public NewTopic createFileParserTopic() {
		return TopicBuilder.name(fileParserTopic).partitions(fileParserTopicPartitions).replicas(fileParserTopicReplicas).build();
	}

	@Bean
	public ProducerFactory<String, ImportRecord> metadataProducerFactory() {
		return new DefaultKafkaProducerFactory<>(metadataProducerConfigs());
	}

	@Bean(name = "fileParserKafkaTemplate")
	public KafkaTemplate<String, ImportRecord> metadataKafkaTemplate() {
		return new KafkaTemplate<>(metadataProducerFactory());
	}

	@Bean
	public NewTopic createEnrichmentTopic() {
		return TopicBuilder.name(this.enrichmentTopic)
				.partitions(this.enrichmentTopicPartitions)
				.replicas(this.enrichmentTopicReplicas).build();
	}

	@Bean
	public ProducerFactory<String, MetadataWrapper> enrichmentProducerFactory() {
		return new DefaultKafkaProducerFactory<>(metadataProducerConfigs());
	}

	@Bean(name = "enrichmentKafkaTemplate")
	public KafkaTemplate<String, MetadataWrapper> enrichmentKafkaTemplate() {
		return new KafkaTemplate<>(enrichmentProducerFactory());
	}
}