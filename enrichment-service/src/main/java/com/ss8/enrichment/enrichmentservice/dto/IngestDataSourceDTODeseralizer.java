package com.ss8.enrichment.enrichmentservice.dto;

import java.io.IOException;

import org.springframework.boot.jackson.JsonComponent;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.ss8.metahub.shared.dto.IngestDataSourceDTO;


@JsonComponent
public class IngestDataSourceDTODeseralizer extends JsonDeserializer<IngestDataSourceDTO>{

	@Override
	public IngestDataSourceDTO deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		IngestDataSourceDTO  dto  = new IngestDataSourceDTO();
		JsonNode node =  jsonParser.getCodec().readTree(jsonParser);
		if(null !=  node.get("id")){
		   dto.setId(node.get("id").intValue());
		}
		if(null !=  node.get("name")){
			 dto.setName(node.get("name").asText());
		}
		if(null !=  node.get("desc")){
			 dto.setDesc(node.get("desc").asText());
		}
		if(null !=  node.get("eventType")){
			dto.setEventType(node.get("eventType").asText());
		}
		if(null !=  node.get("fileType")){
			dto.setFileType(node.get("fileType").asText());
		}
		if(null !=  node.get("streamType")){
			dto.setStreamType(node.get("streamType").asText());
		}
		if(null !=  node.get("userName")){
			dto.setUserName(node.get("userName").asText());
		}
		if(null !=  node.get("importColumnsToTypeMap")){
			 dto.setImportColumnsToTypeMap( node.get("importColumnsToTypeMap").toString());
		}
		if (null != node.get("usStyleDateFormat")) {
			dto.setUsStyleDateFormat((node.get("usStyleDateFormat").asBoolean()));
		}
		if (null != node.get("datasourceType")) {
			dto.setDatasourceType((node.get("datasourceType").asText()));
		}
		if (null !=  node.get("canDelete")){
			 dto.setCanDelete((node.get("canDelete").asBoolean()));
		}
		if (null !=  node.get("metadataFileRequired")){
			 dto.setMetadataFileRequired((node.get("metadataFileRequired").asBoolean()));
		}
		if (null !=  node.get("allowZipFiles")){
			 dto.setAllowZipFiles((node.get("allowZipFiles").asBoolean()));
		}
		
		return dto;
	}

}
