package com.ss8.enrichment.enrichmentservice.service;

import java.util.List;

import com.ss8.enrichment.enrichmentservice.dto.BSAGeoData;
import com.ss8.enrichment.enrichmentservice.dto.CoordinatesRequest;

public interface EnrichmentLookupService {
	
	public List<BSAGeoData> getLatLongCoordinates(List<CoordinatesRequest> coordinatesRequests);
	
	public List<String> getFeature(String cellJsonArray);
	
	public List<String> getPointOfPopulation(String cellJsonArray);

}
