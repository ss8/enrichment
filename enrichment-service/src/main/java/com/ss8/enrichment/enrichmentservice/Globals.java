package com.ss8.enrichment.enrichmentservice;

public class Globals {
	// REST API URL paths (used throughout)
	public static final String REST_API_ALL = "/metahub/api/v1/**";
	public static final String REST_API_URL_BASE = "/metahub/api/v1/enrichment";
	public static final String REST_API_QUERY_CONFIG = (REST_API_URL_BASE + "/queryConfig");
	public static final String REST_API_DATA_SOURCE = (REST_API_URL_BASE + "/ingestdatasource");
	
}
