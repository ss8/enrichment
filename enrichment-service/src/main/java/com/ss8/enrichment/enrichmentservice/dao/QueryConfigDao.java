package com.ss8.enrichment.enrichmentservice.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.annotation.PostConstruct;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ss8.iql.query.dto.Field;
import com.ss8.iql.query.dto.FieldType;
import com.ss8.iql.query.dto.Option;
import com.ss8.iql.query.dto.Options;
import com.ss8.shared.dao.BaseJdbcDao;

@Repository("queryConfigDao")
public class QueryConfigDao<T> extends BaseJdbcDao<T> {
	
	private static final String SELECT_FIELD_DATATYPE_MAPPING = "select field, sourceNames, displayName, fieldType, options, childType from query_fields where collection = 'enrichment'";

	@PostConstruct
	public void init() {
	}

	private static final class FieldRowMapper implements RowMapper<Field> {

		private LinkedHashMap<String, Field> fieldsMap;

		public FieldRowMapper(final LinkedHashMap<String, Field> fieldsMap) {
			this.fieldsMap = fieldsMap;
		}

		public Field mapRow(ResultSet rs, int rowNum) throws SQLException {
			Field field = new Field();
			field.setFieldName(rs.getString("field"));
			field.setFieldType(rs.getString("fieldType"));
			field.setDisplayName(rs.getString("displayName"));
			ArrayList<String> sourceNamesList = new ArrayList<String>(2);
			if (rs.getString("sourceNames") != null) {
				String[] sourceNames = rs.getString("sourceNames").split(",");
				for (String sourceName : sourceNames) {
					sourceNamesList.add(sourceName);
				}
			} else {
				sourceNamesList.add(rs.getString("field"));
			}
			field.setSourceNames(sourceNamesList);
			if (null != rs.getString("childType")) {
				field.setChildType(rs.getString("childType"));
				field.setChild(true);
			}
			String options = rs.getString("options");
			if (null != rs.getString("fieldType") && null != options && options.trim().length() > 0) {
				FieldType fieldType = new FieldType();
				fieldType.setBaseType("Enum");
				ArrayList<String> enumOperators = new ArrayList<String>(4);
				enumOperators.add("equals");
				enumOperators.add("notEqual");
				enumOperators.add("in");
				enumOperators.add("notIn");
				fieldType.setOperators(enumOperators);
				Options fieldTypeOptions = new Options();
				for (String option : options.split(",")) {
					Option fieldTypeOption = new Option();
					fieldTypeOption.setDisplayName(option);
					fieldTypeOptions.setOptionsMap(option, fieldTypeOption);
				}
				fieldType.setOptions(fieldTypeOptions);
			}
			fieldsMap.put(rs.getString("field"), field);
			return field;
		}
	}

	public LinkedHashMap<String, Field> getSchemaFieldsForElasticCollection() throws IOException {
		LinkedHashMap<String, Field> fieldsMap = new LinkedHashMap<String, Field>();
		jdbcTemplate.query(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(SELECT_FIELD_DATATYPE_MAPPING);
				return ps;
			}
		}, new FieldRowMapper(fieldsMap));
		return fieldsMap;
	}

}
