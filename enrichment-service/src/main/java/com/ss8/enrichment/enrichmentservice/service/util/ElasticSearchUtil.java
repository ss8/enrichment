package com.ss8.enrichment.enrichmentservice.service.util;

import java.util.LinkedList;
import java.util.List;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;

public class ElasticSearchUtil {

	public static List<String> getIdsOfData(SearchResponse searchResponse) {
		List<String> ids = new LinkedList<String>();
		SearchHits hits = searchResponse.getHits();
		SearchHit[] searchHits = hits.getHits();
		for (SearchHit hit : searchHits)
			ids.add(hit.getId());
		return ids;
	}
}
