package com.ss8.enrichment.enrichmentservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ss8.enrichment.enrichmentservice.Globals;
import com.ss8.enrichment.enrichmentservice.service.QueryConfigService;
import com.ss8.iql.query.dto.QueryBuilderConfig;

@RestController
public class QueryConfigController {

    @Autowired
    private QueryConfigService queryConfigService;

    @GetMapping(value = Globals.REST_API_QUERY_CONFIG, produces = "application/json")
    public @ResponseBody QueryBuilderConfig getQueryConfig() throws Exception {
        return (QueryBuilderConfig) queryConfigService.getQueryConfig();
    }
}
