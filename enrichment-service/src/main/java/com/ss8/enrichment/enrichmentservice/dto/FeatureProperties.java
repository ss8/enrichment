package com.ss8.enrichment.enrichmentservice.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeatureProperties {

	@SerializedName("id")
	@Expose
	private String id;
	@SerializedName("srs")
	@Expose
	private String srs;
	@SerializedName("type")
	@Expose
	private String type;
	@SerializedName("cellid")
	@Expose
	private String cellid;
	@SerializedName("address")
	@Expose
	private String address;
	@SerializedName("metadata")
	@Expose
	private String metadata;
	@SerializedName("valid_to")
	@Expose
	private String validTo;
	@SerializedName("valid_from")
	@Expose
	private String validFrom;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSrs() {
		return srs;
	}

	public void setSrs(String srs) {
		this.srs = srs;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCellid() {
		return cellid;
	}

	public void setCellid(String cellid) {
		this.cellid = cellid;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public String getValidTo() {
		return validTo;
	}

	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	public String getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

}
