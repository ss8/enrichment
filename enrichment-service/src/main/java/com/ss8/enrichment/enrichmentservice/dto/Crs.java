package com.ss8.enrichment.enrichmentservice.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Crs {

	@SerializedName("type")
	@Expose
	private String type;
	@SerializedName("properties")
	@Expose
	private CrsProperties properties;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public CrsProperties getProperties() {
		return properties;
	}

	public void setProperties(CrsProperties properties) {
		this.properties = properties;
	}

}
