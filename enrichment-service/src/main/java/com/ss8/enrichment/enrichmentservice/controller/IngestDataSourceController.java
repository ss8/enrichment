package com.ss8.enrichment.enrichmentservice.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ss8.enrichment.enrichmentservice.Globals;
import com.ss8.enrichment.enrichmentservice.service.DataImportService;
import com.ss8.metahub.shared.dto.DataImportModelDto;
import com.ss8.metahub.shared.dto.DataSourceEventDTO;
import com.ss8.metahub.shared.dto.DataSourceTypeDTO;
import com.ss8.metahub.shared.dto.EventsResponseDto;
import com.ss8.metahub.shared.dto.GenericResponse;
import com.ss8.metahub.shared.dto.ImportEventDto;
import com.ss8.metahub.shared.dto.ImportStatus;
import com.ss8.metahub.shared.dto.ImportStatusDTO;
import com.ss8.metahub.shared.dto.IngestDataImportDTO;
import com.ss8.metahub.shared.dto.IngestDataSourceDTO;
import com.ss8.metahub.shared.dto.PaginationDTO;
import com.ss8.shared.dao.SharedImportDao;
import com.ss8.shared.dao.IngestDataSourceDAO;
import com.ss8.shared.service.FileStorageService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Data Source and Data Import for Enrichment", description = "Data Import and Data Source for Enrichment")
@RestController
public class IngestDataSourceController {

	private static final String REST_API_DATA_SOURCE_ID = Globals.REST_API_DATA_SOURCE + "/{id}";
	private static final String REST_API_DATA_SOURCE_IMPORT = Globals.REST_API_DATA_SOURCE + "/dataimport";
	private static final String REST_API_DATA_SOURCE_IMPORT_GET_ALL = Globals.REST_API_DATA_SOURCE + "/dataimport/getAll";
	private static final String REST_API_DATA_SOURCE_IMPORT_ID = REST_API_DATA_SOURCE_IMPORT + "/{id}";
	private static final String REST_API_DATA_SOURCES_FOR_IMPORT_ID = REST_API_DATA_SOURCE_ID +"/dataimport";
	private static final String REST_API_DATA_SOURCE_EVENT_TYPES = Globals.REST_API_DATA_SOURCE + "/eventtypes";
	private static final String REST_API_DATA_SOURCE_TYPES = Globals.REST_API_DATA_SOURCE + "/datasourcetypes";
	private static final String REST_API_DATA_SOURCES_FOR_IMPORT_STATUS = REST_API_DATA_SOURCE_IMPORT_ID +"/importStatus";
	private static final Logger logger = LoggerFactory.getLogger(IngestDataSourceController.class);
	
	@Autowired
    ApplicationEventPublisher applicationEventPublisher;

	@Autowired
	IngestDataSourceDAO dao;
	
	@Autowired
	DataImportService dataImportService;
	
	@Autowired
	FileStorageService fileUploadService;
	
	@Autowired
	SharedImportDao importDao;
	
    @Operation
	@Tag(name = "Post data for creating Data source", description = "Return the Data source object after creation")
	@PostMapping(value = Globals.REST_API_DATA_SOURCE, produces = "application/json")
	public @ResponseBody EventsResponseDto<IngestDataSourceDTO> addDataSource(@RequestHeader("x-auth-username") String userName, @RequestBody IngestDataSourceDTO request) throws Exception {
		List<IngestDataSourceDTO> list = new ArrayList<IngestDataSourceDTO>();
		request.setUserName(userName);
		IngestDataSourceDTO dto = dataImportService.addDataSource(request);
		list.add(dto);
		if (request.getStreamType() != null && !request.getStreamType().isEmpty() && !dataImportService.processDataStreamTask(dto)) {
			list.remove(dto);
		}
		EventsResponseDto<IngestDataSourceDTO> response = new EventsResponseDto<IngestDataSourceDTO>(list, 1, null);
		return response;
	}

    @Operation
   	@Tag(name = "get all  Data source's ", description = "Return the list of  Data source objects")
	@GetMapping(value = Globals.REST_API_DATA_SOURCE, produces = "application/json")
	public @ResponseBody EventsResponseDto<IngestDataSourceDTO> getDataSources() throws IOException, JSONException {
    	
		List<IngestDataSourceDTO> list = dao.getDataSources();
		int size = list == null ? -1 : list.size() ;
		EventsResponseDto<IngestDataSourceDTO> response = new EventsResponseDto<IngestDataSourceDTO>(list, size, null);
		return response;
	}
    @Operation
   	@Tag(name = "get data of Data source by id", description = "Return the Data source object By Id")
	@GetMapping(value = REST_API_DATA_SOURCE_ID, produces = "application/json")
	public @ResponseBody EventsResponseDto<IngestDataSourceDTO> getDataSourcebyID(@PathVariable("id") int id) throws IOException, JSONException {
		List<IngestDataSourceDTO> list = dao.getDataSourcebyId(id);
		int size = list == null ? 0 : list.size();
		EventsResponseDto<IngestDataSourceDTO> response = new EventsResponseDto<IngestDataSourceDTO>(list, size, null);
		return response;
	}
    @Operation
   	@Tag(name = "Delete a Data source", description = "delete datasource  by id")
	@DeleteMapping(value = REST_API_DATA_SOURCE_ID, produces = "application/json")
	public @ResponseBody EventsResponseDto<IngestDataSourceDTO> deleteDataSource(@PathVariable("id") int id) throws Exception {
    	List<IngestDataSourceDTO> list = dao.getDataSourcebyId(id);
    	dao.deleteDataSource(id);
		logger.info("Publishing import event to notify the import event listener to refresh query config cache");
		applicationEventPublisher.publishEvent(new ImportEventDto());
		logger.info("Deleting kafka topic if the datasource was of streaming type");
		if(list != null && list.size()>0) {
			IngestDataSourceDTO dto = list.get(0);
			if(!"".equals(dto.getStreamType())){
				dataImportService.processDataStreamDelete(dto);
			}
		}
		EventsResponseDto<IngestDataSourceDTO> response = new EventsResponseDto<IngestDataSourceDTO>(null, 0, null);
		return response;
	}
	
	/*
	 *   Data Import Rest API	
	 */
	@PostMapping(value = REST_API_DATA_SOURCE_IMPORT , produces = "application/json" , consumes = "multipart/form-data")
	public @ResponseBody EventsResponseDto<IngestDataImportDTO> addDataImport(@RequestHeader("x-auth-username") String userName, @ModelAttribute DataImportModelDto request) throws Exception {
		List<IngestDataImportDTO> list = new ArrayList<IngestDataImportDTO>();
		List<IngestDataSourceDTO> datasourcesList = dao.getDataSourcebyId(request.getDatasource());
		if (datasourcesList != null && datasourcesList.size() > 0) {
			String fileLoc = fileUploadService.storeFile(request.getFile(), datasourcesList.get(0).getDatasourceFolderName());
			IngestDataImportDTO dto = new IngestDataImportDTO();
			dto.setName(request.getName());
			dto.setDescription(request.getDescription());
			dto.setDatasource(request.getDatasource());
			dto.setUserName(userName);
			dto.setFilename(fileLoc);
			dto.setStatus(ImportStatus.SUBMITTED.toString());
			list.add(dao.addDataImport(dto));
			dataImportService.processDataImportTask(dto, datasourcesList.get(0));
		}
		EventsResponseDto<IngestDataImportDTO> response = new EventsResponseDto<IngestDataImportDTO>(list, 1, null);
		return response;
	}
	
	@PostMapping(value = REST_API_DATA_SOURCE_IMPORT_GET_ALL, produces = "application/json")
	public @ResponseBody EventsResponseDto<IngestDataImportDTO> getDataImports(@RequestBody PaginationDTO paginationDTO) throws IOException, JSONException {
		  String  orderBy = " ";
		  if(paginationDTO.getSortModel() == null){
			  orderBy = orderBy +" id desc";
		  }else {
			  orderBy = orderBy +  paginationDTO.getSortModel().getColId() +"  " + paginationDTO.getSortModel().getSort();
		  }
		List<IngestDataImportDTO> list = dao.getDataImports(orderBy,paginationDTO.getStartRow(),paginationDTO.getEndRow());
		int size = list == null ? -1 : list.size() < (paginationDTO.getEndRow() - paginationDTO.getStartRow()) ? list.size() : -1 ;
		EventsResponseDto<IngestDataImportDTO> response = new EventsResponseDto<IngestDataImportDTO>(list, size, null);
		return response;
	}

	@GetMapping(value = REST_API_DATA_SOURCE_IMPORT_ID, produces = "application/json")
	public @ResponseBody EventsResponseDto<IngestDataImportDTO> getDataImportbyID(@PathVariable("id") int id) throws IOException, JSONException {
		List<IngestDataImportDTO> list = dao.getDataImportbyId(id);
		int size = list == null ? 0 : list.size();
		EventsResponseDto<IngestDataImportDTO> response = new EventsResponseDto<IngestDataImportDTO>(list, size, null);
		return response;
	}
	
	@PostMapping(value = REST_API_DATA_SOURCES_FOR_IMPORT_ID, produces = "application/json")
	public @ResponseBody EventsResponseDto<IngestDataImportDTO> getDataImportbyDataSourceID(@PathVariable("id") int id ,@RequestBody PaginationDTO paginationDTO) throws IOException, JSONException {
		  String  orderBy = " ";
		  if(paginationDTO.getSortModel() == null){
			  orderBy = orderBy +" id desc";
			  
		  }else {
			  orderBy = orderBy +  paginationDTO.getSortModel().getColId() +"  " + paginationDTO.getSortModel().getSort();
			  
		  }
		List<IngestDataImportDTO> list = dao.getDataImportbyDataSourceId(id ,orderBy,paginationDTO.getStartRow(),paginationDTO.getEndRow() );
		int size = list == null ? -1 : list.size() < (paginationDTO.getEndRow() - paginationDTO.getStartRow()) ? list.size() : -1 ;
		EventsResponseDto<IngestDataImportDTO> response = new EventsResponseDto<IngestDataImportDTO>(list, size, null);
		return response;
	}

	@DeleteMapping(value = REST_API_DATA_SOURCE_IMPORT_ID, produces = "application/json")
	public @ResponseBody EventsResponseDto<IngestDataImportDTO> deleteDataImport(@PathVariable("id") int id) throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("deleteDataImport: id - " + id);
		}
		
		dataImportService.scheduleDataImportDelete(id);
		EventsResponseDto<IngestDataImportDTO> response = new EventsResponseDto<IngestDataImportDTO>(null, 0, null);
		return response;
	}
	
	// event types
	@Operation
    @Tag(name = "Post data for creating new event type", description = "Return the new event type  object after creation")
	@PostMapping(value = REST_API_DATA_SOURCE_EVENT_TYPES, produces = "application/json")
	public @ResponseBody EventsResponseDto<DataSourceEventDTO> addDataSourceEventType(@RequestBody DataSourceEventDTO request) throws IOException, JSONException {
		List<DataSourceEventDTO> list = new ArrayList<DataSourceEventDTO>();
		list.add(dao.addDataSourceEventType(request));
		EventsResponseDto<DataSourceEventDTO> response = new EventsResponseDto<DataSourceEventDTO>(list, 1, null);
		return response;
	}

	 @Operation
		@Tag(name = "get all  Data source  event types ", description = "Return the list of  Eventtype  objects")
	@GetMapping(value = REST_API_DATA_SOURCE_EVENT_TYPES, produces = "application/json")
	public @ResponseBody EventsResponseDto<DataSourceEventDTO> getDataSourceEventTypes() throws IOException, JSONException {
		List<DataSourceEventDTO> list = dao.getDataSourceEventTypes();
		int size = list == null ? 0 : list.size();
		EventsResponseDto<DataSourceEventDTO> response = new EventsResponseDto<DataSourceEventDTO>(list, size, null);
		return response;
	}
	 
	@Operation
	@Tag(name = "get all  Data source types ", description = "Return the list of datasource type objects")
	@GetMapping(value = REST_API_DATA_SOURCE_TYPES, produces = "application/json")
	public @ResponseBody EventsResponseDto<DataSourceTypeDTO> getDataSourceTypes()
			throws IOException, JSONException {
		List<DataSourceTypeDTO> list = dao.getDataSourceTypes();
		int size = list == null ? 0 : list.size();
		EventsResponseDto<DataSourceTypeDTO> response = new EventsResponseDto<DataSourceTypeDTO>(list, size, null);
		return response;
	}
	
	@Operation
	@Tag(name = "Update Import Status ", description = "Update the import status of the corresponding import along with updating any total record, failed record count and any error messages")
	@PostMapping(value = REST_API_DATA_SOURCES_FOR_IMPORT_STATUS, produces = "application/json")
	public @ResponseBody ResponseEntity<GenericResponse> updateDataImportStatusByImportId(@PathVariable("id") int id, 
			@RequestBody ImportStatusDTO importStatusDTO) throws IOException, JSONException {
		try {
			dataImportService.updateDataImportStatus(id, importStatusDTO);
			return new ResponseEntity<GenericResponse>(new GenericResponse("Import status updated successfully", HttpStatus.OK.value()), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<GenericResponse>(new GenericResponse("Import status not updated successfully, because of the error - " 
					+ e.getMessage(), HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
		}
	}
}
