package com.ss8.enrichment.enrichmentservice.config;

import com.ss8.enrichment.enrichmentservice.Globals;

public final class GlobalConstants {
	public static final String REST_API_GET_LAT_LONG_FEATURE = Globals.REST_API_URL_BASE + "/getlatlongfeature";
	
	public static final String REST_API_GET_FEATURE = Globals.REST_API_URL_BASE + "/getfeature";
	
	public static final String REST_API_GET_POP = Globals.REST_API_URL_BASE + "/getpointofpopulation";

	public static final String REST_API_GET_DNS_NAMES = Globals.REST_API_URL_BASE + "/getdnsnames";
	
	public static final String FIELD_CELL_ID = "cellId";
	public static final String FIELD_DNS_IP = "ip";
	public static final String FIELD_DNS_NAME = "dnsName";
	public static final String DNSLOOKUP = "DNS";

	public static String FIELD_EVENT_DATE = "eventDate";
	
	public static String FIELD_CELL_TYPE = "cellType";
	
	public static String FIELD_ADDRESS = "physicalAddress";
	
	public static String FIELD_VALID_FROM = "validFrom";
	
	public static String FIELD_VALID_TO = "validTo";
	
	public static String FIELD_LATLONG = "eventLocations";
	
	public static String FIELD_GEOMETRY = "geometry";
	
	public static String FIELD_GEOMETRY_COORDINATES = "coordinates";
	
	public static String FIELD_METADATA = "metadata_txt";
	
	public static String ERROR_CELLTOWER_NOT_FOUND = "cell tower data not found";
	
	public static String ERROR_EVENTID_INVALID = "event id invalid";
	
	public static String ERROR_EVENTDATE_INVALID = "event date is invalid";
	
	public static String CELLTYPE_CGI = "CGI";

	public static String CELLTYPE_SAI = "SAI";
	
	public static String CELLTYPE_RAI = "RAI";
	
	public static String CELLTYPE_TAI = "TAI";
	
	public static String CELLTYPE_ECGI = "ECGI";
	
	public static String CELLTYPE_LAI = "LAI";
	
	public static String CRS_TYPE = "EPSG";
	
	public static String CRS_CODE = "4326";
	
	public static String FEATUREJSON_TYPE = "FeatureCollection";
	
	public static String GEOFEATURE_ID = "cell-coverage";
	
	public static String POPFEATURE_ID = "point-of-population";
	
	public static String FEATURE = "Feature";
	
	public static String GEO_POINT = "Point";
	
	public static String GEO_POLYGON = "Polygon";
	
	public static String GEO_NAME = "poly_geom";
	
	public static String POP = "POP";
	
	public static String BSA = "BSA";
	
	public static String POP_DATASOURCE_TYPE = "Point of Population";
	
	public static String CELLTYPE_NCGI = "NCGI";

}
