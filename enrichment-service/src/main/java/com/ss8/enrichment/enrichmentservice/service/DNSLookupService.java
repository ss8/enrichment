package com.ss8.enrichment.enrichmentservice.service;

import com.ss8.enrichment.enrichmentservice.dto.DNSLookup;

import java.util.List;
import java.util.Set;

public interface DNSLookupService {
    Set<DNSLookup> findDNSMatches(List<String> dnsLookupList);
}
