package com.ss8.enrichment.enrichmentservice.controller;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import com.ss8.enrichment.enrichmentservice.dto.DNSLookup;
import com.ss8.enrichment.enrichmentservice.service.DNSLookupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ss8.enrichment.enrichmentservice.config.GlobalConstants;
import com.ss8.enrichment.enrichmentservice.dto.BSAGeoData;
import com.ss8.enrichment.enrichmentservice.dto.CoordinatesRequest;
import com.ss8.enrichment.enrichmentservice.service.EnrichmentLookupService;

/**
 * Rest Endpoint Controller Class
 * @author MVallabhan
 *
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class EnrichmentlookupController {

	@Autowired
	private EnrichmentLookupService enrichmentlookupService;

	@Autowired
	private DNSLookupService dnsLookupService;
	
	private final static ObjectMapper objectMapper = new ObjectMapper();

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrichmentlookupController.class);

	@PostMapping(value = GlobalConstants.REST_API_GET_LAT_LONG_FEATURE, produces = "application/json")
	public ResponseEntity<String> getLatLongFeature(@RequestBody(required = true) List<CoordinatesRequest> inputFeatures) {
		HttpHeaders headers = new HttpHeaders();
		LOGGER.debug("BSA Lookup Request Received:: {} ", inputFeatures);
		Instant starttime = Instant.now();
		Instant endtime = null;
		try {
			
			List<BSAGeoData> bsaGeoList = enrichmentlookupService.getLatLongCoordinates(inputFeatures);
			return new ResponseEntity<String>(
					objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(bsaGeoList), headers, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("BSA Lookup Exception occured ", e.getMessage());
			return new ResponseEntity<String>("Request could not be processed", headers,
					HttpStatus.valueOf(422));
		} finally {
			endtime = Instant.now();
			LOGGER.debug("Time taken BSA lookup = " + (endtime.toEpochMilli() - starttime.toEpochMilli()));
		}

	}
	
	/**
	 * 
	 * @param cellJsonArray
	 * @return
	 */
	@PostMapping(value = GlobalConstants.REST_API_GET_FEATURE, produces = "application/json")
	public ResponseEntity<String> getFeature(@RequestBody(required = true) String cellJsonArray) {
		HttpHeaders headers = new HttpHeaders();
		LOGGER.debug("BSA Lookup Request Received:: {} ", cellJsonArray);
		try {
			List<String> featureResponse = enrichmentlookupService.getFeature(cellJsonArray);
			return new ResponseEntity<String>(featureResponse.toString(), headers, HttpStatus.OK );
		} catch (Exception e) {
			LOGGER.error("BSA Lookup Exception occured ", e.getMessage());
			return new ResponseEntity<String>("{\n" + "\"Code\" : " +"422" + "," + "\n\"Message\" : " +"\"" +e +"\"" +"\n}", headers, HttpStatus.valueOf(422));
		}


	}
	
	/**
	 * 
	 * @param cellJsonArray
	 * @return
	 */
	@PostMapping(value = GlobalConstants.REST_API_GET_POP, produces = "application/json")
	public ResponseEntity<String> getPointOfPopulation(@RequestBody(required = true) String cellJsonArray) {
		HttpHeaders headers = new HttpHeaders();
		LOGGER.debug("POP Lookup Request Received:: {} ", cellJsonArray);
		try {
			List<String> popResponse = enrichmentlookupService.getPointOfPopulation(cellJsonArray);
			return new ResponseEntity<String>(popResponse.toString(), headers, HttpStatus.OK );
		} catch (Exception e) {
			LOGGER.error("POP Lookup Exception occured ", e.getMessage());
			return new ResponseEntity<String>("{\n" + "\"Code\" : " +"422" + "," + "\n\"Message\" : " +"\"" +e +"\"" +"\n}", headers, HttpStatus.valueOf(422));
		}


	}

	@PostMapping(value = GlobalConstants.REST_API_GET_DNS_NAMES, produces = "application/json")
	public ResponseEntity<Object> getDNSNames(@RequestBody(required = true) List<String> dnsList) {
		HttpHeaders headers = new HttpHeaders();
		LOGGER.debug("DNS Lookup Request Received:: {} ", dnsList);
		try {
			Set<DNSLookup> popResponse = dnsLookupService.findDNSMatches(dnsList);
			return new ResponseEntity<>(popResponse, headers, HttpStatus.OK );
		} catch (Exception e) {
			LOGGER.error("DNS Lookup Exception occured ", e);
			return new ResponseEntity<>("{\n" + "\"Code\" : " +"422" + "," + "\n\"Message\" : " +"\"" +e +"\"" +"\n}", headers, HttpStatus.valueOf(422));
		}


	}
}
