package com.ss8.enrichment.enrichmentservice;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.Security;

import org.bouncycastle.jcajce.provider.BouncyCastleFipsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.cassandra.CassandraDataAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;

@Configuration
@EnableKafka
@ComponentScan(basePackages = {"com.ss8.shared", "com.ss8.enrichment"})
@SpringBootApplication(scanBasePackages = {"com"}, exclude = {JpaRepositoriesAutoConfiguration.class, CassandraDataAutoConfiguration.class, KafkaAutoConfiguration.class})
public class EnrichmentApplication {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrichmentApplication.class);

	public static void main(String[] args) throws UnknownHostException {
		// Add Bouncy Castle FIPS provider
        Security.addProvider(new BouncyCastleFipsProvider());
        
		String hostName = InetAddress.getLocalHost().getHostName();
    	System.setProperty("log.name", hostName);
        SpringApplication.run(EnrichmentApplication.class, args);
        LOGGER.info("Starting Enrichment Service API Application....");
	}

}
 	