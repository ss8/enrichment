package com.ss8.enrichment.enrichmentservice.config;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.ss8.enrichment.enrichmentservice.config.JdbcConfiguration;
import com.ss8.metahub.shared.util.JasyptPasswordCryptor;
import com.ss8.metahub.shared.util.PasswordEncrypter;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
public class JdbcConfiguration {
	
	static final Logger log = LoggerFactory.getLogger(JdbcConfiguration.class);

	@Value("${spring.datasource.driver-class-name}")
	private String driver;

	@Value("${spring.datasource.password}")
	private String encodedPassword;

	@Value("${spring.datasource.enrichment.url}")
	private String url;
	
	@Value("${spring.datasource.username}")
	private String username;
	
	@Value("${spring.datasource.hikari.maximumPoolSize:5}")
	private int poolSize;
	
	@Value("${spring.datasource.hikari.connection-timeout:30000}")
	private long connectionTimeout;
	
	@Primary
	@Bean(name = "dataSource")
	public DataSource dataSource() {
		String password = JasyptPasswordCryptor.getInstance()
				.decrypt(encodedPassword);
		if (log.isInfoEnabled()) {
			log.info("Database configuration: D=" + driver + "\nURL=" + url);
		}
		 
	     HikariDataSource dataSource = new HikariDataSource();
	        dataSource.setDriverClassName(driver);
	        dataSource.setJdbcUrl(url);
	        dataSource.setUsername(username);
	        dataSource.setPassword(password);
	        dataSource.addDataSourceProperty("cachePrepStmts", true);
	        dataSource.addDataSourceProperty("prepStmtCacheSize", 25000);
	        dataSource.addDataSourceProperty("prepStmtCacheSqlLimit", 20048);
	        dataSource.addDataSourceProperty("useServerPrepStmts", true);
	        dataSource.addDataSourceProperty("initializationFailFast", true);
	        dataSource.setPoolName("MYSQL_CONNECTION_POOL");
	        dataSource.setMaximumPoolSize(poolSize);
	        dataSource.setConnectionTimeout(this.connectionTimeout);
	        return dataSource;
	 }
	
	@Bean(name = "jdbcCommonDBTemplate") 
    public JdbcTemplate jdbcTemplate(@Qualifier("dataSource")DataSource dataSourceSFW) { 
        return new JdbcTemplate(dataSourceSFW); 
    } 
    
    @Bean(name = "jdbcCommonDBNamedTemplate") 
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate(@Qualifier("dataSource")DataSource dataSourceSFW){
       return new NamedParameterJdbcTemplate(dataSourceSFW);	
    }

}
