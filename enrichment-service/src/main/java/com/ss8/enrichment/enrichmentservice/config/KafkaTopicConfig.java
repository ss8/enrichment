package com.ss8.enrichment.enrichmentservice.config;

import java.util.Properties;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.apache.kafka.clients.admin.AdminClient;

@Configuration
public class KafkaTopicConfig {
	@Value("${spring.kafka.bootstrap-servers}")
	private String bootstrapServers;
	
	@Bean
	public AdminClient AdminClient() {
		Properties config = new Properties();
	    config.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
	    return AdminClient.create(config);
	}
}