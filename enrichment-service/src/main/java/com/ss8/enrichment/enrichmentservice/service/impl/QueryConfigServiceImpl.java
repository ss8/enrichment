package com.ss8.enrichment.enrichmentservice.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.ss8.enrichment.enrichmentservice.dao.QueryConfigDao;
import com.ss8.enrichment.enrichmentservice.dto.DataSource;
import com.ss8.enrichment.enrichmentservice.service.QueryConfigService;
import com.ss8.iql.query.dto.EventTypes;
import com.ss8.iql.query.dto.Field;
import com.ss8.iql.query.dto.Fields;
import com.ss8.iql.query.dto.QueryBuilderConfig;
import com.ss8.iql.query.dto.SourceType;

@Service
public class QueryConfigServiceImpl implements QueryConfigService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(QueryConfigServiceImpl.class);

	private static ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private ResourceLoader resourceLoader;

	@Autowired
	private QueryConfigDao queryConfigDao;

	@Autowired
	private CacheManager cacheManager;

	private QueryBuilderConfig queryConfig = null;

	@Value("${enrichment.datasources}")
	private String enrichmentDataSources;

	@Value("${enrichment.queryConfigFileLocation}")
	private String queryConfigFileLocation;

	@Value("${enrichment.queryconfig.field.exclusions:null}")
	private String fieldExclusions;

	@Cacheable("queryConfig")
	public QueryBuilderConfig getQueryConfig() throws IOException {
		this.retrieveLatestQueryConfig();
		return this.queryConfig;
	}

	public void flushAndFetchQueryConfig() throws IOException {
		this.evictQueryConfigCacheAtIntervals();
	}

	public synchronized void retrieveLatestQueryConfig() throws IOException {

		TypeFactory typeFactory = mapper.getTypeFactory();
		List<DataSource> dataSourcesList = mapper.readValue(enrichmentDataSources,
				typeFactory.constructCollectionType(List.class, DataSource.class));
		LOGGER.debug("DataSources List :: "+dataSourcesList);
	
		if (dataSourcesList != null && dataSourcesList.size() > 0) {
			Set<String> fieldExclusionsList = new HashSet<String>();
			if (fieldExclusions != null) {
				for (String excludedField : fieldExclusions.split(",")) {
					fieldExclusionsList.add(excludedField);
				}
			}
			
			// Populate the query conf
			QueryBuilderConfig baseQueryConfig = populateBaseQueryConfig();
			System.out.println("dataSourcesList - " + dataSourcesList.size());
			for (DataSource dataSource : dataSourcesList) {
				Map<String, SourceType> sourcesTypesMap = baseQueryConfig.getSourceTypes().getSourceTypesMap();
				SourceType sourceType = sourcesTypesMap.get("fusion");

				Fields fields = new Fields();
				sourceType.setFields(fields);

				EventTypes eventTypes = sourceType.getEventTypes();
				HashSet<String> fieldNamesSet = new HashSet<String>();
				for (String source : dataSource.getSources()) {
					LinkedHashMap<String, Field> fieldsMap = this.queryConfigDao.getSchemaFieldsForElasticCollection();
					System.out.println("fieldsMap - " + fieldsMap.size());
					for (String field : fieldsMap.keySet()) {
						if (!fieldNamesSet.contains(field) && !fieldExclusionsList.contains(field)) {
							fields.setFieldsMap(field, fieldsMap.get(field));
							eventTypes.getEventTypesMap().get("all").getFields().add(field);
							fieldNamesSet.add(field);
						}
					}
					eventTypes.getEventTypesMap().get("all").getSourceNames().add(source);
				}
			}
			this.updateQueryConfigCache(baseQueryConfig);
		}

	}

	@CachePut(value = "queryConfig")
	public void updateQueryConfigCache(final QueryBuilderConfig queryConfig) {
		this.queryConfig = queryConfig;
	}

	public QueryBuilderConfig populateBaseQueryConfig() throws IOException {
		QueryBuilderConfig baseQueryConfig = null;
		File file = new File(queryConfigFileLocation);
		String json = null;
		if (file.exists()) {
			json = FileUtils.readFileToString(file, Charset.defaultCharset());
		} else {
			Resource resource = resourceLoader.getResource("classpath:" + queryConfigFileLocation);
			InputStream in = resource.getInputStream();
			json = IOUtils.toString(in, Charset.defaultCharset());
			in.close();
		}
		baseQueryConfig = mapper.readValue(json, QueryBuilderConfig.class);
		return baseQueryConfig;
	}

	@Scheduled(fixedRateString = "${enrichment.queryconfig.cache.refreshRate}")
	public synchronized void evictQueryConfigCacheAtIntervals() throws IOException {
		if (cacheManager.getCache("queryConfig") != null) {
			cacheManager.getCache("queryConfig").clear();
		}
		this.retrieveLatestQueryConfig();
	}

}
