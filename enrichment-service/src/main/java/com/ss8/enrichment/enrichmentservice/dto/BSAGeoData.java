package com.ss8.enrichment.enrichmentservice.dto;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BSAGeoData {
	
	@JsonProperty("event_id")
	private String eventId;
	
	private double[] coordinates;
	
	private String address;

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public double[] getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(double[] coordinates) {
		this.coordinates = coordinates;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BSAGeoData [eventId=").append(eventId).append(", coordinates=")
				.append(Arrays.toString(coordinates)).append(", address=").append(address).append("]");
		return builder.toString();
	}
	
	

}
