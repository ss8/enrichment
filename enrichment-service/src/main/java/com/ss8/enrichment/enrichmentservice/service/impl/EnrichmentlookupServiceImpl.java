package com.ss8.enrichment.enrichmentservice.service.impl;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.ss8.enrichment.enrichmentservice.config.ElasticSearchConfiguration;
import com.ss8.enrichment.enrichmentservice.config.GlobalConstants;
import com.ss8.enrichment.enrichmentservice.dto.BSAGeoData;
import com.ss8.enrichment.enrichmentservice.dto.CoordinatesRequest;
import com.ss8.enrichment.enrichmentservice.dto.Crs;
import com.ss8.enrichment.enrichmentservice.dto.CrsProperties;
import com.ss8.enrichment.enrichmentservice.dto.Feature;
import com.ss8.enrichment.enrichmentservice.dto.FeatureJson;
import com.ss8.enrichment.enrichmentservice.dto.FeatureProperties;
import com.ss8.enrichment.enrichmentservice.dto.Geometry;
import com.ss8.enrichment.enrichmentservice.dto.PopFeature;
import com.ss8.enrichment.enrichmentservice.dto.PopProperties;
import com.ss8.enrichment.enrichmentservice.service.EnrichmentLookupService;
import com.ss8.iql.query.ElasticQueryWrapper;

@Service
public class EnrichmentlookupServiceImpl implements EnrichmentLookupService {

	@Value("${elastic.enrichment.indexname:enrichment}")
	private String elasticIndexNameEnrichment;
	
	@Value("${enrichment.batchSize:1000}")
	private int batchSize;

	@Autowired
	private ElasticSearchConfiguration elasticConfig;

	private static ObjectMapper objectMapper = new ObjectMapper();

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrichmentlookupServiceImpl.class);
	
	@Autowired
    @Qualifier("fixedThreadPool")
    private ExecutorService executorService;
	
	@Override
	@SuppressWarnings("unchecked")
	public List<BSAGeoData> getLatLongCoordinates(List<CoordinatesRequest> coordinatesRequests) {
		final AtomicInteger counter = new AtomicInteger(0);
		//Split main list into small list based on batchSize and then process in different threads.
		Collection<List<CoordinatesRequest>> listOfCoordsReqList = coordinatesRequests.stream().collect(Collectors.groupingBy(s -> counter.getAndIncrement()/batchSize)).values();
		CountDownLatch countDownLatch = new CountDownLatch(1);
		List<CompletableFuture<List<BSAGeoData>>> future = listOfCoordsReqList.stream().map(item -> CompletableFuture.supplyAsync(() -> this.getLatLongCoordinatesAsync(item), executorService)).collect(Collectors.toList());
		countDownLatch.countDown();
		return future.stream().map(CompletableFuture::join).flatMap(List::stream).collect(Collectors.toList());		
	}

	
	@SuppressWarnings("unchecked")
	public List<BSAGeoData> getLatLongCoordinatesAsync(List<CoordinatesRequest> coordinatesRequests) {

		/*
		 * Create a temporary Map<cellId, List<CoordinatesRequest>>. This will help when
		 * building response back during bulk requests and if the cellid is not in the
		 * Elastic. If the cell Id is not in the request then substitute it with eventId
		 * to build the empty response. Storing the value as List would help us to
		 * determine later which requests were processed and which were'nt.
		 */
		Instant starttime = null;
		Instant endtime = null;
		
		Map<String, List<CoordinatesRequest>> cellIdCoordReqMap = coordinatesRequests.stream().map(coordreq -> {
			if (coordreq.getCellId() == null) {
				coordreq.setCellId(coordreq.getEventId());
			}
			return coordreq;
		}).collect(Collectors.groupingBy(CoordinatesRequest::getCellId,
				Collectors.mapping(Function.identity(), Collectors.toList())));

		/*
		 * Create a temporary Response Map to be used when cellType is not provided in
		 * the Request. This will be used to set the values in the Response object for
		 * requests with no cellType.
		 */
		Map<String, Object> cellIdCoordRespMap = new HashMap<>();
		List<BSAGeoData> bsaGeoList = new ArrayList<BSAGeoData>();
		/* Build a set which will be used to form Elastic request will all cellIds. */
		Set<String> cellIdsSet = coordinatesRequests.stream().map(e -> e.getCellId()).collect(Collectors.toSet());
		starttime = Instant.now();
		SearchResponse results = this.searchEnrichmentIndex(cellIdsSet, GlobalConstants.BSA);
		endtime = Instant.now();
		LOGGER.debug("Time taken Elastic lookup = " + (endtime.toEpochMilli() - starttime.toEpochMilli()));
		double[] coordinatesValue = null;
		SearchHits hits = results.getHits();
		SearchHit[] searchHits = hits.getHits();
		/* Traverse each searchResult */
		for (SearchHit hit : searchHits) {
			Map<String, Object> hitMap = hit.getSourceAsMap();
			String cellIdValue = (String) hitMap.get(GlobalConstants.FIELD_CELL_ID);
			String cellTypeValue = (String) hitMap.get(GlobalConstants.FIELD_CELL_TYPE);
			String addressValue = (String) hitMap.get(GlobalConstants.FIELD_ADDRESS);
			List<Double> geoObjList = (List<Double>) hitMap.get(GlobalConstants.FIELD_LATLONG);
			if (null != geoObjList) {
				Collections.reverse(geoObjList);
				coordinatesValue = geoObjList.stream().mapToDouble(Double::doubleValue).toArray();
			}
			LocalDateTime validFromValue = getDateFormats((String) hitMap.get(GlobalConstants.FIELD_VALID_FROM));
			LocalDateTime validToValue = getDateFormats((String) hitMap.get(GlobalConstants.FIELD_VALID_TO));

			if (cellIdCoordReqMap.containsKey(cellIdValue)) {
				int cellIdCoordReqMapSize = cellIdCoordReqMap.get(cellIdValue).size();
				/* Traverse each coordinateRequest Object for that cellId */
				for (int i = cellIdCoordReqMapSize - 1; i >= 0; i--) {
					LocalDateTime eventDate = getDateFormats(cellIdCoordReqMap.get(cellIdValue).get(i).getEventdate());
					boolean isAfterOrEqual = eventDate.isAfter(validFromValue) || eventDate.isEqual(validFromValue);
					boolean isBeforeOrEqual = eventDate.isBefore(validToValue) || eventDate.isEqual(validToValue);

					boolean isCellTypeMatch = false;
					if (StringUtils.isNotBlank(cellIdCoordReqMap.get(cellIdValue).get(i).getCellType())) {
						if (cellIdCoordReqMap.get(cellIdValue).get(i).getCellType().trim()
								.equals(cellTypeValue.trim())) {
							isCellTypeMatch = true;
						} else {
							// continue to see if there is a next match for cellType.
							continue;
						}
					} else {
						cellIdCoordRespMap = handleEmptyCellType(cellIdCoordRespMap, cellIdValue, cellTypeValue,
								hitMap);
					}

					if (isAfterOrEqual && isBeforeOrEqual) {

						BSAGeoData bsaGeoData = new BSAGeoData();
						bsaGeoData.setAddress(addressValue);
						bsaGeoData.setEventId(cellIdCoordReqMap.get(cellIdValue).get(i).getEventId());
						bsaGeoData.setCoordinates(coordinatesValue);
						if (isCellTypeMatch) {
							bsaGeoList.add(bsaGeoData);
							/* Remove that particular coordinateRequest object */
							cellIdCoordReqMap.get(cellIdValue).remove(i);
						} else {
							Map<String, Object> cellIdObjMap = (Map<String, Object>) cellIdCoordRespMap
									.get(cellIdValue);
							String newAddressValue = (String) cellIdObjMap.get(GlobalConstants.FIELD_ADDRESS);
							geoObjList = (List<Double>) cellIdObjMap.get(GlobalConstants.FIELD_LATLONG);
							if (null != geoObjList) {
								coordinatesValue = geoObjList.stream().mapToDouble(Double::doubleValue).toArray();
							}
							boolean isCellIdInBsaList = false;
							final double[] newCoordinatesValue = coordinatesValue;
							if (bsaGeoList != null) {
								for (BSAGeoData bsaGeo : bsaGeoList) {
									if (bsaGeo.getEventId()
											.equals(cellIdCoordReqMap.get(cellIdValue).get(i).getEventId())) {
										bsaGeo.setCoordinates(newCoordinatesValue);
										bsaGeo.setAddress(newAddressValue);
										isCellIdInBsaList = true;
										break;
									}
								}

							}
							if (!isCellIdInBsaList) {
								bsaGeoList.add(bsaGeoData);
								/* Remove that particular coordinateRequest object */
								cellIdCoordReqMap.get(cellIdValue).remove(i);
							}
						}
					}
				}

			}

			// Reset coordinates value back to null
			coordinatesValue = null;

		}

		if (!cellIdCoordRespMap.isEmpty()) {
			for (String cellId : cellIdCoordRespMap.keySet()) {
				int cellIdCoordReqMapSize = cellIdCoordReqMap.get(cellId).size();
				for (int i = cellIdCoordReqMapSize - 1; i >= 0; i--) {
					/* Only remove where cellType is null */
					if (null == cellIdCoordReqMap.get(cellId).get(i).getCellType()) {
						cellIdCoordReqMap.remove(cellId);
					}
				}
			}
		}

		// By this time, if there are any cellIds that were not in Elastic, will be in
		// this map. Just send empty eventIds for that.
		if (!cellIdCoordReqMap.isEmpty()) {
			for (Map.Entry<String, List<CoordinatesRequest>> entry : cellIdCoordReqMap.entrySet()) {
				for (int i = 0; i < entry.getValue().size(); i++) {
					BSAGeoData bsaGeoData = new BSAGeoData();
					bsaGeoData.setEventId(entry.getValue().get(i).getEventId());
					bsaGeoList.add(bsaGeoData);
					LOGGER.debug("Cell Id for the event Id: " + entry.getValue().get(i).getEventId()
							+ " is not found in the index.");
				}
			}
		}
		
		return bsaGeoList;
	}

	private LocalDateTime getDateFormats(String dateString) {
		try {
			OffsetDateTime odt = OffsetDateTime.parse(dateString);
			LocalDateTime dateTimeWithoutOffset = odt.withOffsetSameInstant(ZoneOffset.UTC).toLocalDateTime();
			return dateTimeWithoutOffset;
		} catch (Exception e) {
			LOGGER.error(LocalDateTime.now() + " :: " + dateString + " :: This Date Format is Not Supported!..");
			return null;
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<String> getFeature(String cellJsonArray) {
		List<String> geoJsonStringList = new ArrayList<>();
		try {

			List<CoordinatesRequest> cellTowerList = objectMapper.readValue(cellJsonArray,
					new TypeReference<List<CoordinatesRequest>>() {
					});

			// Create a temporary Map<cellId, CoordinatesRequest>. This will help when
			// building response back during bulk requests and if the cellid is not in the
			// Elastic.
			Map<String, CoordinatesRequest> cellIdCoordReqMap = cellTowerList.stream()
					.collect(Collectors.toMap(CoordinatesRequest::getCellId, Function.identity()));
			Set<String> cellIdsSet = cellTowerList.stream().filter(e -> e.getCellId() != null).map(e -> e.getCellId()).collect(Collectors.toSet());

			List<Double> geoPolyList = null;
			SearchResponse results = this.searchEnrichmentIndexWithGeometry(cellIdsSet, GlobalConstants.BSA);
			SearchHits hits = results.getHits();
			SearchHit[] searchHits = hits.getHits();
			for (SearchHit hit : searchHits) {
				Map<String, Object> hitMap = hit.getSourceAsMap();
				String cellIdValue = (String) hitMap.get(GlobalConstants.FIELD_CELL_ID);
				String cellTypeValue = (String) hitMap.get(GlobalConstants.FIELD_CELL_TYPE);
				String addressValue = (String) hitMap.get(GlobalConstants.FIELD_ADDRESS);
				List<Double> geoPointList = (List<Double>) hitMap.get(GlobalConstants.FIELD_LATLONG);
				String metadataTxt = (String) hitMap.get(GlobalConstants.FIELD_METADATA);
				Map<String, Object> geoPolyMap = (Map<String, Object>) hitMap.get(GlobalConstants.FIELD_GEOMETRY);
				if (null != geoPolyMap) {
					geoPolyList = (List<Double>) geoPolyMap.get(GlobalConstants.FIELD_GEOMETRY_COORDINATES);
				}
				LocalDateTime validFromValue = getDateFormats((String) hitMap.get(GlobalConstants.FIELD_VALID_FROM));
				LocalDateTime validToValue = getDateFormats((String) hitMap.get(GlobalConstants.FIELD_VALID_TO));

				if (cellIdCoordReqMap.containsKey(cellIdValue)) {
					if (!StringUtils.isNotBlank(cellIdCoordReqMap.get(cellIdValue).getEventId())) {
						String errMsg = errorResponse(cellIdCoordReqMap.get(cellIdValue).getCellId(),
								cellIdCoordReqMap.get(cellIdValue).getEventId(), GlobalConstants.ERROR_EVENTID_INVALID);
						geoJsonStringList.add(errMsg);
						cellIdCoordReqMap.remove(cellIdValue);
						continue;
					}
					String eventDateStr = cellIdCoordReqMap.get(cellIdValue).getEventdate();
					// Validate eventDate. Safe to assume that the offset sign - or + occurs after
					// the 10th character.
					if (!StringUtils.isNotBlank(eventDateStr)
							|| (!eventDateStr.substring(10).contains("-") && !eventDateStr.contains("+"))) {
						String errMsg = errorResponse(cellIdCoordReqMap.get(cellIdValue).getCellId(),
								cellIdCoordReqMap.get(cellIdValue).getEventId(),
								GlobalConstants.ERROR_EVENTDATE_INVALID);
						geoJsonStringList.add(errMsg);
						cellIdCoordReqMap.remove(cellIdValue);
						continue;
					}
					// Event date format for this API is 2023-08-30 18:06:00-0400. Convert it into
					// regular format without offset
					LocalDateTime date = LocalDateTime.parse(eventDateStr,
							DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ssZ"));
					LocalDateTime eventDate = getDateFormats(
							DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'").format(date));

					boolean isAfterOrEqual = eventDate.isAfter(validFromValue) || eventDate.isEqual(validFromValue);
					boolean isBeforeOrEqual = eventDate.isBefore(validToValue) || eventDate.isEqual(validToValue);

					boolean isCellTypeMatch = false;
					if (StringUtils.isNotBlank(cellIdCoordReqMap.get(cellIdValue).getCellType())) {
						if (cellIdCoordReqMap.get(cellIdValue).getCellType().trim().equals(cellTypeValue.trim())) {
							isCellTypeMatch = true;
						}
					} else {
						// set the flag to true so that celltype match is not performed, only cellid and
						// event date match is performed.
						isCellTypeMatch = true;
					}

					if (isAfterOrEqual && isBeforeOrEqual && isCellTypeMatch) {
						FeatureJson featureJson = new FeatureJson();
						Crs crs = new Crs();
						crs.setType(GlobalConstants.CRS_TYPE);
						CrsProperties crsProperties = new CrsProperties();
						crsProperties.setCode(GlobalConstants.CRS_CODE);
						crs.setProperties(crsProperties);
						featureJson.setCrs(crs);
						featureJson.setType(GlobalConstants.FEATUREJSON_TYPE);

						Feature geoPointfeature = new Feature();
						geoPointfeature.setId(GlobalConstants.GEOFEATURE_ID + "." + cellIdValue);
						FeatureProperties geoPointfeatureProperties = new FeatureProperties();
						geoPointfeatureProperties.setCellid(cellIdValue);
						geoPointfeatureProperties.setSrs(GlobalConstants.CRS_TYPE + ":" + GlobalConstants.CRS_CODE);
						geoPointfeatureProperties.setType(cellTypeValue);
						geoPointfeatureProperties.setAddress(addressValue);
						geoPointfeatureProperties.setValidFrom(validFromValue.toString());
						geoPointfeatureProperties.setValidTo(validToValue.toString());
						geoPointfeatureProperties.setMetadata(metadataTxt);
						geoPointfeature.setProperties(geoPointfeatureProperties);
						geoPointfeature.setType(GlobalConstants.FEATURE);
						Geometry geoPointGeometry = new Geometry();
						geoPointGeometry.setType(GlobalConstants.GEO_POINT);
						geoPointGeometry.setCoordinates(geoPointList);
						geoPointfeature.setGeometry(geoPointGeometry);

						Feature geoPolyfeature = new Feature();
						geoPolyfeature.setId(GlobalConstants.GEOFEATURE_ID + ".1");
						geoPolyfeature.setType(GlobalConstants.FEATURE);
						FeatureProperties geoPolyfeatureProperties = new FeatureProperties();
						geoPolyfeatureProperties.setCellid(cellIdValue);
						geoPolyfeature.setProperties(geoPolyfeatureProperties);
						Geometry geoPolyGeometry = new Geometry();
						geoPolyGeometry.setType(GlobalConstants.GEO_POLYGON);
						geoPolyGeometry.setCoordinates(geoPolyList);
						geoPolyfeature.setGeometry(geoPolyGeometry);
						geoPolyfeature.setGeometryName(GlobalConstants.GEO_NAME);

						List<Feature> featureList = new ArrayList<>();
						featureList.add(geoPointfeature);
						featureList.add(geoPolyfeature);
						featureJson.setFeatures(featureList);
						Gson gson = new Gson();
						gson.toJson(featureJson);
						geoJsonStringList.add(gson.toJson(featureJson));
						cellIdCoordReqMap.remove(cellIdValue);
					}
				}

			}

			// By this time, if there are any cellIds that were not in Elastic, will be in
			// this map. Just send empty eventIds for that.
			if (!cellIdCoordReqMap.isEmpty()) {
				for (Map.Entry<String, CoordinatesRequest> entry : cellIdCoordReqMap.entrySet()) {
					String errMsg = errorResponse(entry.getValue().getCellId(), entry.getValue().getEventId(),
							GlobalConstants.ERROR_CELLTOWER_NOT_FOUND);
					geoJsonStringList.add(errMsg);

				}
			}

		} catch (JsonMappingException e) {
			LOGGER.error("There is a JsonMappingException happened. The exception is " + e);
		} catch (JsonProcessingException e) {
			LOGGER.error("There is a JsonProcessingException happened. The exception is " + e);
		}

		return geoJsonStringList;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<String> getPointOfPopulation(String cellJsonArray) {
		List<String> popJsonStringList = new ArrayList<>();
		try {

			List<CoordinatesRequest> cellTowerList = objectMapper.readValue(cellJsonArray,
					new TypeReference<List<CoordinatesRequest>>() {
					});

			// Create a temporary Map<cellId, CoordinatesRequest>. This will help when
			// building response back during bulk requests and if the cellid is not in the
			// Elastic.
			Map<String, CoordinatesRequest> cellIdCoordReqMap = cellTowerList.stream()
					.collect(Collectors.toMap(CoordinatesRequest::getCellId, Function.identity()));
			Set<String> cellIdsSet = cellTowerList.stream().filter(e -> e.getCellId() != null).map(e -> e.getCellId()).collect(Collectors.toSet());

			List<Double> geoPolyList = null;
			SearchResponse results = this.searchEnrichmentIndex(cellIdsSet, GlobalConstants.POP_DATASOURCE_TYPE);
			SearchHits hits = results.getHits();
			SearchHit[] searchHits = hits.getHits();
			for (SearchHit hit : searchHits) {
				Map<String, Object> hitMap = hit.getSourceAsMap();
				String cellIdValue = (String) hitMap.get(GlobalConstants.FIELD_CELL_ID);
				List<Double> geoPointList = (List<Double>) hitMap.get(GlobalConstants.FIELD_LATLONG);

				if (cellIdCoordReqMap.containsKey(cellIdValue)) {
					if (!StringUtils.isNotBlank(cellIdCoordReqMap.get(cellIdValue).getEventId())) {
						String errMsg = errorResponse(cellIdCoordReqMap.get(cellIdValue).getCellId(),
								cellIdCoordReqMap.get(cellIdValue).getEventId(), GlobalConstants.ERROR_EVENTID_INVALID);
						popJsonStringList.add(errMsg);
						cellIdCoordReqMap.remove(cellIdValue);
						continue;
					}

					PopFeature popFeature = new PopFeature();
					popFeature.setId(GlobalConstants.POPFEATURE_ID + "." + cellIdValue);
					popFeature.setType(GlobalConstants.FEATURE);
					Geometry popPointGeometry = new Geometry();
					popPointGeometry.setType(GlobalConstants.GEO_POINT);
					popPointGeometry.setCoordinates(geoPointList);
					popFeature.setGeometry(popPointGeometry);
					PopProperties popProperties = new PopProperties();
					popProperties.setCellid(cellIdValue);
					popProperties.setType(GlobalConstants.POP);
					popFeature.setProperties(popProperties);

					Gson gson = new Gson();
					popJsonStringList.add(gson.toJson(popFeature));
					cellIdCoordReqMap.remove(cellIdValue);

				}

			}

			// By this time, if there are any cellIds that were not in Elastic, will be in
			// this map. Just send empty eventIds for that.
			if (!cellIdCoordReqMap.isEmpty()) {
				for (Map.Entry<String, CoordinatesRequest> entry : cellIdCoordReqMap.entrySet()) {
					String errMsg = errorResponse(entry.getValue().getCellId(), entry.getValue().getEventId(),
							GlobalConstants.ERROR_CELLTOWER_NOT_FOUND);
					popJsonStringList.add(errMsg);

				}
			}

		} catch (JsonMappingException e) {
			LOGGER.error("There is a JsonMappingException happened. The exception is " + e);
		} catch (JsonProcessingException e) {
			LOGGER.error("There is a JsonProcessingException happened. The exception is " + e);
		}

		return popJsonStringList;
	}	

	private SearchResponse searchEnrichmentIndex(Set<String> cellIdsSet, String dataSourceType) {
		ElasticQueryWrapper elasticQueryWrapper = elasticConfig.getElasticInstance();
		BoolQueryBuilder querybuilder = new BoolQueryBuilder();
		querybuilder.filter(QueryBuilders.termsQuery(com.ss8.metahub.shared.util.GlobalConstants.FIELD_DATA_SOURCE_TYPE, dataSourceType));
		querybuilder.filter(QueryBuilders.termsQuery(GlobalConstants.FIELD_CELL_ID, cellIdsSet));
		LOGGER.debug("Elastic query for BSA Lookup is " + querybuilder.toString());

		String[] includeColumns = new String[] { GlobalConstants.FIELD_CELL_ID, GlobalConstants.FIELD_CELL_TYPE,
				GlobalConstants.FIELD_LATLONG, GlobalConstants.FIELD_ADDRESS, GlobalConstants.FIELD_VALID_FROM,
				GlobalConstants.FIELD_VALID_TO};

		try {
			SearchResponse results = elasticQueryWrapper.queryForDocsFromElastic(querybuilder, 0, 10000, includeColumns,
					null, elasticIndexNameEnrichment);
			return results;
		} catch (IOException e) {
			LOGGER.error("There is an IOException happened. The exception is " + e);
			return null;
		}
	}
	

	private String errorResponse(String cellId, String eventId, String errorMsg) {
		StringBuilder sb = new StringBuilder();
		sb.append("{\"cell_id\":\"" + cellId + "\"," + "\"errormsg\":\"" + errorMsg + "\"" + "\n," + "\"event_id\":\""
				+ eventId + "\"\n}");
		return sb.toString();
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> handleEmptyCellType(Map<String, Object> cellIdCoordRespMap, String cellIdValue,
			String cellTypeValue, Map<String, Object> hitMap) {

		if (!cellIdCoordRespMap.isEmpty()) {
			Map<String, Object> cellTypeObjMap = (Map<String, Object>) cellIdCoordRespMap.get(cellIdValue);
			if (cellTypeObjMap != null) {
				String coordrespMapCellType = (String) cellTypeObjMap.get(GlobalConstants.FIELD_CELL_TYPE);
				// Check for the order CGI, SAI, RAI, TAI, ECGI, LAI.
				if (GlobalConstants.CELLTYPE_CGI.equals(coordrespMapCellType)) {
					// Don't do anything.
				} else if (GlobalConstants.CELLTYPE_SAI.equals(coordrespMapCellType)) {
					// Check if the current hitMap is CGI
					if (GlobalConstants.CELLTYPE_CGI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					}
				} else if (GlobalConstants.CELLTYPE_RAI.equals(coordrespMapCellType)) {
					// Check if the current hitMap is CGI
					if (GlobalConstants.CELLTYPE_CGI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					} else if (GlobalConstants.CELLTYPE_SAI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					}
				} else if (GlobalConstants.CELLTYPE_TAI.equals(coordrespMapCellType)) {
					// Check if the current hitMap is CGI
					if (GlobalConstants.CELLTYPE_CGI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					} else if (GlobalConstants.CELLTYPE_SAI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					} else if (GlobalConstants.CELLTYPE_RAI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					}
				} else if (GlobalConstants.CELLTYPE_ECGI.equals(coordrespMapCellType)) {
					// Check if the current hitMap is CGI
					if (GlobalConstants.CELLTYPE_CGI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					} else if (GlobalConstants.CELLTYPE_SAI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					} else if (GlobalConstants.CELLTYPE_RAI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					} else if (GlobalConstants.CELLTYPE_TAI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					}
				} else if (GlobalConstants.CELLTYPE_LAI.equals(coordrespMapCellType)) {
					// Check if the current hitMap is CGI
					if (GlobalConstants.CELLTYPE_CGI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					} else if (GlobalConstants.CELLTYPE_SAI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					} else if (GlobalConstants.CELLTYPE_RAI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					} else if (GlobalConstants.CELLTYPE_TAI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					} else if (GlobalConstants.CELLTYPE_ECGI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					}
				} else if (GlobalConstants.CELLTYPE_NCGI.equals(coordrespMapCellType)) {
					// Check if the current hitMap is CGI
					if (GlobalConstants.CELLTYPE_CGI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					} else if (GlobalConstants.CELLTYPE_SAI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					} else if (GlobalConstants.CELLTYPE_RAI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					} else if (GlobalConstants.CELLTYPE_TAI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					} else if (GlobalConstants.CELLTYPE_ECGI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					} else if (GlobalConstants.CELLTYPE_LAI.equals(cellTypeValue.trim())) {
						cellIdCoordRespMap.put(cellIdValue, hitMap);
					}
				}
			} else {
				// This will add if there is no entry for a cellId in the Map.
				cellIdCoordRespMap.put(cellIdValue, hitMap);
			}

		} else {
			// This will add if there is no entry in the Map.
			cellIdCoordRespMap.put(cellIdValue, hitMap);
		}

		return cellIdCoordRespMap;

	}
	
	private SearchResponse searchEnrichmentIndexWithGeometry(Set<String> cellIdsSet, String dataSourceType) {
		ElasticQueryWrapper elasticQueryWrapper = elasticConfig.getElasticInstance();
		BoolQueryBuilder querybuilder = new BoolQueryBuilder();
		querybuilder.filter(QueryBuilders.termsQuery(com.ss8.metahub.shared.util.GlobalConstants.FIELD_DATA_SOURCE_TYPE, dataSourceType));
		querybuilder.filter(QueryBuilders.termsQuery(GlobalConstants.FIELD_CELL_ID, cellIdsSet));
		LOGGER.debug("Elastic query for BSA Lookup is " + querybuilder.toString());

		String[] includeColumns = new String[] { GlobalConstants.FIELD_CELL_ID, GlobalConstants.FIELD_CELL_TYPE,
				GlobalConstants.FIELD_LATLONG, GlobalConstants.FIELD_ADDRESS, GlobalConstants.FIELD_VALID_FROM,
				GlobalConstants.FIELD_VALID_TO, GlobalConstants.FIELD_GEOMETRY, GlobalConstants.FIELD_METADATA };

		try {
			SearchResponse results = elasticQueryWrapper.queryForDocsFromElastic(querybuilder, 0, 10000, includeColumns,
					null, elasticIndexNameEnrichment);
			return results;
		} catch (IOException e) {
			LOGGER.error("There is an IOException happened. The exception is " + e);
			return null;
		}
	}

}
