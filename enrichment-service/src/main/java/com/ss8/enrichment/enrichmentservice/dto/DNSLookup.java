package com.ss8.enrichment.enrichmentservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DNSLookup {

    private String ip;
    private String dnsName;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDnsName() {
        return dnsName;
    }

    public void setDnsName(String dnsName) {
        this.dnsName = dnsName;
    }

    @Override
    public String toString() {
        return "DNSLookup{" +
                "ip='" + ip + '\'' +
                ", dnsName='" + dnsName + '\'' +
                '}';
    }
}
