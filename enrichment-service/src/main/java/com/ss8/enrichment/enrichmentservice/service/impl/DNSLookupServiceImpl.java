package com.ss8.enrichment.enrichmentservice.service.impl;

import com.ss8.enrichment.enrichmentservice.config.ElasticSearchConfiguration;
import com.ss8.enrichment.enrichmentservice.config.GlobalConstants;
import com.ss8.enrichment.enrichmentservice.dto.DNSLookup;
import com.ss8.enrichment.enrichmentservice.service.DNSLookupService;
import com.ss8.iql.query.ElasticQueryWrapper;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class DNSLookupServiceImpl implements DNSLookupService {

    @Value("${enrichment.batchSize:1000}")
    private int batchSize;

    @Autowired
    @Qualifier("fixedThreadPool")
    private ExecutorService executorService;

    @Autowired
    private ElasticSearchConfiguration elasticConfig;

    @Value("${elastic.enrichment.indexname:enrichment}")
    private String elasticIndexNameEnrichment;

    private static final Logger LOGGER = LoggerFactory.getLogger(DNSLookupServiceImpl.class);

    //@Override
    public Set<DNSLookup> findDNSMatches(List<String> dnsLookupList) {
        final AtomicInteger counter = new AtomicInteger(0);
        //Split main list into small list based on batchSize and then process in different threads.
        Collection<List<String>> torNodeReqList = dnsLookupList.stream().filter(ip -> ip != null).collect(Collectors.groupingBy(s -> counter.getAndIncrement()/batchSize)).values();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Set<CompletableFuture<Set<DNSLookup>>> future = torNodeReqList.stream().map(item -> CompletableFuture.supplyAsync(() -> this.findDNSAsync(item), executorService)).collect(Collectors.toSet());
        countDownLatch.countDown();
        return future.stream().map(CompletableFuture::join).flatMap(Set::stream).collect(Collectors.toSet());
    }

    private Set<DNSLookup> findDNSAsync(List<String> ipSet) {

        BoolQueryBuilder querybuilder = new BoolQueryBuilder();
        querybuilder.filter(QueryBuilders.termQuery(com.ss8.metahub.shared.util.GlobalConstants.FIELD_DATA_SOURCE_TYPE, GlobalConstants.DNSLOOKUP));
        querybuilder.filter(QueryBuilders.termsQuery(GlobalConstants.FIELD_DNS_IP, ipSet));

        return getTorNodesFromElastic(querybuilder);
    }

    private Set<DNSLookup> getTorNodesFromElastic(BoolQueryBuilder query) {
        ElasticQueryWrapper elasticQueryWrapper = elasticConfig.getElasticInstance();
        LOGGER.debug("Elastic query for TOR Node Lookup is " + query.toString());
        Set<DNSLookup> matched = new HashSet<>();
        try {
            SearchResponse results = elasticQueryWrapper.queryForDocsFromElastic(query, 0, 10000, null,
                    null, elasticIndexNameEnrichment);

            SearchHits hits = results.getHits();
            SearchHit[] searchHits = hits.getHits();

            for (SearchHit hit : searchHits) {
                Map<String, Object> hitMap = hit.getSourceAsMap();
                DNSLookup dns = new DNSLookup();
                dns.setIp((String) hitMap.get(GlobalConstants.FIELD_DNS_IP));
                dns.setDnsName((String) hitMap.get(GlobalConstants.FIELD_DNS_NAME));
                matched.add(dns);
            }
            return matched;
        } catch (IOException e) {
            LOGGER.error("There is an IOException happened. The exception is " + e);
            return null;
        }
    }
}
