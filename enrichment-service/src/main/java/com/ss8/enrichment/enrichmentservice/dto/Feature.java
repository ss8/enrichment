package com.ss8.enrichment.enrichmentservice.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Feature {

	@SerializedName("id")
	@Expose
	private String id;
	@SerializedName("type")
	@Expose
	private String type;
	@SerializedName("geometry")
	@Expose
	private Geometry geometry;
	@SerializedName("properties")
	@Expose
	private FeatureProperties properties;
	@SerializedName("geometry_name")
	@Expose
	private String geometryName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Geometry getGeometry() {
		return geometry;
	}

	public void setGeometry(Geometry geometry) {
		this.geometry = geometry;
	}

	public FeatureProperties getProperties() {
		return properties;
	}

	public void setProperties(FeatureProperties properties) {
		this.properties = properties;
	}

	public String getGeometryName() {
		return geometryName;
	}

	public void setGeometryName(String geometryName) {
		this.geometryName = geometryName;
	}

}
