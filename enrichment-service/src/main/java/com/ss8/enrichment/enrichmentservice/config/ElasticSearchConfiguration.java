package com.ss8.enrichment.enrichmentservice.config;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AbstractFactoryBean;
import org.springframework.context.annotation.Configuration;

import com.ss8.iql.query.ElasticQueryWrapper;
import com.ss8.iql.query.dto.ElasticServer;

@Configuration
public class ElasticSearchConfiguration extends AbstractFactoryBean<RestHighLevelClient> {

	@Value("${enrichment.elastic.ip:localhost}")
	private String enrichmentHost;

	@Value("${enrichment.elastic.port:12913}")
	private int enrichmentPort;

	private RestHighLevelClient restHighLevelClient;
	
	private static ElasticQueryWrapper eqw = null;

	private static final Logger LOG = LoggerFactory.getLogger(ElasticSearchConfiguration.class);

	public ElasticQueryWrapper getElasticInstance() {
		if(eqw != null) {
			return eqw;
		}
		eqw = ElasticQueryWrapper.getInstance();
		List<ElasticServer> servers = new ArrayList<>(); // list of servers ElasticServer
		ElasticServer methubInstance = new ElasticServer(enrichmentHost, enrichmentPort);
		servers.add(methubInstance);
		eqw.RegisterElasticServers(servers); // register the servers
		return eqw;
	}

	@Override
	public void destroy() {
		try {
			if (restHighLevelClient != null) {
				restHighLevelClient.close();
			}
		} catch (final Exception e) {
			LOG.error("Error closing ElasticSearch client: ", e);
		}
	}

	@Override
	public Class<RestHighLevelClient> getObjectType() {
		return RestHighLevelClient.class;
	}

	@Override
	public boolean isSingleton() {
		return false;
	}

	@Override
	public RestHighLevelClient createInstance() {
		return buildClient();
	}

	private RestHighLevelClient buildClient() {
		try {
			restHighLevelClient = new RestHighLevelClient(
					RestClient.builder(new HttpHost(enrichmentHost, enrichmentPort, null)
					// , new HttpHost("10.0.156.154", 9200, "http")
					));
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error(e.getMessage());
		}
		return restHighLevelClient;
	}

}
