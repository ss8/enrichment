package com.ss8.enrichment.enrichmentservice;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import com.ss8.enrichment.enrichmentservice.dto.BSAGeoData;
import com.ss8.enrichment.enrichmentservice.dto.CoordinatesRequest;
import com.ss8.enrichment.enrichmentservice.service.impl.EnrichmentlookupServiceImpl;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertEquals;

public class EnrichmentlookupServiceTest extends EnrichmentApplicationTest {
	
	@Autowired
	EnrichmentlookupServiceImpl enrichmentlookupService;
	
	static final String BSA_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	static final DateTimeFormatter BSA_DATE_FORMATTER = DateTimeFormatter.ofPattern(BSA_DATE_FORMAT);
	
	@Test(description = "Test calling the method getLatLongCoordinates of the Class EnrichmentlookupService")
	public void testGetLatLongCoordinates() {
		
		CoordinatesRequest coordinatesRequest = new CoordinatesRequest();
		coordinatesRequest.setCellId("123-12-12345-12342");
		coordinatesRequest.setEventdate("2021-01-10T02:55:00.000Z");
		coordinatesRequest.setEventId("123");
		List<CoordinatesRequest> coordinatesRequests = new ArrayList<>();
		coordinatesRequests.add(coordinatesRequest);
		List<BSAGeoData> bsaGeoDataList = enrichmentlookupService.getLatLongCoordinates(coordinatesRequests);
		assertEquals("123", bsaGeoDataList.get(0).getEventId());
		
		
	}
	
	@Test(description = "Test calling the method getFeature of the Class EnrichmentlookupService")
	public void testGetFeature() {
		
		String cellJsonString = "[{\r\n"
				+ "    \"cell_id\": \"123-12-12345-12342\",\r\n"
				+ "    \"cell_type\": \"CGI\",\r\n"
				+ "    \"eventdate\": \"2021-01-10 02:55:00+0700\",\r\n"
				+ "    \"event_id\": \"123\"\r\n"
				+ "  }]";
		List<String> geoJsonList = enrichmentlookupService.getFeature(cellJsonString);
		assertNotNull(geoJsonList);
		System.out.println(geoJsonList.get(0));
		
	}
	
	@Test(description = "Test calling the method getFeature of the Class EnrichmentlookupService")
	public void testGetFeatureCellIdNotFound() {
		
		String cellJsonString = "[{\r\n"
				+ "    \"cell_id\": \"123-12-12345-123467\",\r\n"
				+ "    \"cell_type\": \"CGI\",\r\n"
				+ "    \"eventdate\": \"2021-01-10T02:55:00Z\",\r\n"
				+ "    \"event_id\": \"123\"\r\n"
				+ "  }]";
		List<String> geoJsonList = enrichmentlookupService.getFeature(cellJsonString);
		assertNotNull(geoJsonList);
		System.out.println(geoJsonList.get(0));
		
	}
	
	@Test(description = "Test calling the method getFeature of the Class EnrichmentlookupService")
	public void testGetFeatureEventIdInvalid() {
		
		String cellJsonString = "[{\r\n"
				+ "    \"cell_id\": \"123-12-12345-12342\",\r\n"
				+ "    \"cell_type\": \"CGI\",\r\n"
				+ "    \"eventdate\": \"2021-01-10 02:55:00+0700\",\r\n"
				+ "    \"event_id\": \"\"\r\n"
				+ "  }]";
		List<String> geoJsonList = enrichmentlookupService.getFeature(cellJsonString);
		assertNotNull(geoJsonList);
		System.out.println(geoJsonList.get(0));
		
	}
	
	@Test(description = "Test calling the method getFeature of the Class EnrichmentlookupService")
	public void testGetFeatureEventDateInvalid() {
		
		String cellJsonString = "[{\r\n"
				+ "    \"cell_id\": \"123-12-12345-12342\",\r\n"
				+ "    \"cell_type\": \"CGI\",\r\n"
				+ "    \"eventdate\": \"2021-01-10 02:55:00Z\",\r\n"
				+ "    \"event_id\": \"123\"\r\n"
				+ "  }]";
		List<String> geoJsonList = enrichmentlookupService.getFeature(cellJsonString);
		assertNotNull(geoJsonList);
		System.out.println(geoJsonList.get(0));
		
	}
	

}
