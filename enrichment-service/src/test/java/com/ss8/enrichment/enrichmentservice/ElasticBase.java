package com.ss8.enrichment.enrichmentservice;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;


import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.core.CountRequest;
import org.elasticsearch.client.core.CountResponse;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import pl.allegro.tech.embeddedelasticsearch.EmbeddedElastic;
import pl.allegro.tech.embeddedelasticsearch.PopularProperties;


public class ElasticBase extends EnrichmentApplicationTest {
	public ElasticBase() {
		super();
	}


	EmbeddedElastic embeddedElastic = null;


	public static String ELASTIC_VERSION = "7.5.2";
	public static String CLUSTER_NAME = "my_test_cluster";
	public static int PORT = 12913;
	public RestHighLevelClient restHighLevelClient = null;

	private static final String ELASTIC_INSTALL_FILE_PATH = System.getProperty("user.dir") + "\\..\\common-test-resources\\elastic\\elasticsearch-7.5.2.zip";


	@BeforeClass
	public void startElastic() throws IOException, InterruptedException {

		try {

			Files.createDirectories(Paths.get(System.getProperty("user.dir") + "\\bin"));
			Files.createDirectories(Paths.get(System.getProperty("user.dir") + "\\build\\resources\\test\\"));
			copyFile(ELASTIC_INSTALL_FILE_PATH, System.getProperty("user.dir") + "\\bin\\elasticsearch-7.5.2.zip");
			copyFile(ELASTIC_INSTALL_FILE_PATH, System.getProperty("user.dir") + "\\build\\resources\\test\\elasticsearch-7.5.2.zip");
			String path = ptH + "src/test/resources/elastic/schema.json";
			String content = new String(Files.readAllBytes(Paths.get(path)));
			embeddedElastic = EmbeddedElastic.builder().withInResourceLocation("elasticsearch-7.5.2.zip")
					.withSetting(PopularProperties.HTTP_PORT, PORT)
					.withSetting(PopularProperties.CLUSTER_NAME, CLUSTER_NAME).withStartTimeout(60, TimeUnit.SECONDS)
					.withCleanInstallationDirectoryOnStop(true).build().start();
			restHighLevelClient = new RestHighLevelClient(RestClient.builder(new HttpHost("localhost", 12913, null)));
			CreateIndexRequest request = new CreateIndexRequest("enrichment");
			request.source(content, XContentType.JSON);
			CreateIndexResponse result = restHighLevelClient.indices().create(request, RequestOptions.DEFAULT);

		} catch (Exception e) {

			e.printStackTrace();

		}

	}


	public long countElasticDocs(final BoolQueryBuilder query, final String collectionName) throws IOException {
		CountRequest countRequest = new CountRequest(collectionName);
		countRequest.query(query);
		CountResponse countResponse = restHighLevelClient.count(countRequest, RequestOptions.DEFAULT);
		return countResponse.getCount();

	}


	public void deleteDocs(final String indexName) throws IOException, InterruptedException {
		DeleteIndexRequest request = new DeleteIndexRequest(indexName);
		restHighLevelClient.indices().delete(request, RequestOptions.DEFAULT);
		Thread.sleep(10000);
	}


	@AfterClass
	public void stopElastic() {
		embeddedElastic.deleteIndices();
		embeddedElastic.stop();
	}

}
