package com.ss8.enrichment.enrichmentservice.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.FileInputStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ss8.enrichment.enrichmentservice.ElasticBase;
import com.ss8.enrichment.enrichmentservice.Globals;
import com.ss8.metahub.shared.dto.PaginationDTO;

public class IngestDataSourceControllerTest extends ElasticBase {

	@Value("${enrichment.elastic.refeshrate.seconds:2}")
	private long elasticRefreshRate;

	@Value("${enrichment.dataimportdelete.scheduled.wait.seconds:2}")
	private long scheduledWaitTime;

	@BeforeClass
	public void setup() throws Exception {
		
	}
	
	@Test(description = "Add data source which is malformed ")
	public void TestAddingDataSourceMalformed() throws Exception {
		String json = getFileContentAsString(ptH + "src/test/resources/ingestdatasource/json-datasource-malformed.txt");
		restController.perform(post(Globals.REST_API_DATA_SOURCE).content(json).header("Content-Type", "application/json")).andExpect(status().isBadRequest());
	}

	@Test(description = "Add new Data Source")
	public void TestAddingDataSource() throws Exception {
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
		String json = getFileContentAsString(ptH + "src/test/resources/ingestdatasource/json-datasource.txt");
		ResultActions result = restController.perform(post(Globals.REST_API_DATA_SOURCE).content(json).header("Content-Type", "application/json").header("x-auth-username", "a")).andExpect(status().isOk());
		String resultString = result.andReturn().getResponse().getContentAsString();
		System.out.println("DATA::" + resultString);
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
	}
	
	int datasource = 0;
	@Test(description = "get  Data Sources", dependsOnMethods = "TestAddingDataSource")
	public void TestgetAllDataSource() throws Exception {
		ObjectMapper obj_mapper = new ObjectMapper();
		PaginationDTO pg_dto = new PaginationDTO();
		pg_dto.setStartRow(0);
		pg_dto.setEndRow(80);
		String json = obj_mapper.writeValueAsString(pg_dto);
		ResultActions result = restController.perform(get(Globals.REST_API_DATA_SOURCE).content(json).header("Content-Type", "application/json").header("x-auth-username", "a")).andExpect(status().isOk());
		String resultString = result.andReturn().getResponse().getContentAsString();
		System.out.println("DATA::" + resultString);
		String split[] = resultString.split(",");
		String splitId[] = split[3].split(":");
		datasource = Integer.parseInt(splitId[splitId.length - 1]);
	}

	@Test(description = "get single datasource", dependsOnMethods = "TestgetAllDataSource")
	public void TestGetDataSource() throws Exception {
		ResultActions result = restController.perform(get(Globals.REST_API_DATA_SOURCE+"/"+datasource).header("Content-Type", "application/json").header("x-auth-username", "a")).andExpect(status().isOk());
		String resultString = result.andReturn().getResponse().getContentAsString();
		System.out.println("DATA::" + resultString);
	}

	@Test(description = "delete  datasource", dependsOnMethods = "TestGetDataSource")
	public void TestDeleteDataSource() throws Exception {
		ResultActions result = restController.perform(delete(Globals.REST_API_DATA_SOURCE+"/"+datasource).header("Content-Type", "application/json").header("x-auth-username", "a")).andExpect(status().isOk());
		String resultString = result.andReturn().getResponse().getContentAsString();
		System.out.println("DATA::" + resultString);
	}
	
	@Test(description = "get  datasource which is not in database", dependsOnMethods = "TestDeleteDataSource")
	public void TestGetDataSourceNotInDB() throws Exception {
		ResultActions result = restController.perform(get(Globals.REST_API_DATA_SOURCE+"/"+datasource).header("Content-Type", "application/json").header("x-auth-username", "a")).andExpect(status().isOk());
		String resultString = result.andReturn().getResponse().getContentAsString();
		System.out.println("DATA::" + resultString);
		Assert.isTrue(resultString.contains("\"lastRow\":0"), "Checking no data returned");
	}

	@Test(description = "delete  datasource which is not in database" , dependsOnMethods = "TestGetDataSourceNotInDB")
	public void TestDeleteDataSourceNotInDB() throws Exception {
		try {
			ResultActions result = restController.perform(delete(Globals.REST_API_DATA_SOURCE+"/"+datasource).header("Content-Type", "application/json").header("x-auth-username", "a"));
		} catch (Exception e) {
			Assert.isTrue(e.getMessage().contains("Id Not Found Or Unable to delete data for id 1"), "Validating whether we received the correct delete data source message in the exception");
		}
	}
	
	//Unit test code is allready there to test  solr insertion no needed to test it.
	@Test(description = "Add new Data import" , dependsOnMethods = "TestDeleteDataSourceNotInDB")
	public void TestAddingDataImport() throws Exception {
		String json = getFileContentAsString(ptH + "src/test/resources/ingestdatasource/json-datasource.txt");
		ResultActions result = restController.perform(post(Globals.REST_API_DATA_SOURCE).content(json).header("Content-Type", "application/json").header("x-auth-username", "a")).andExpect(status().isOk());
		String resultString = result.andReturn().getResponse().getContentAsString();
		System.out.println("DATA::" + resultString);
		//added data source now add data import
		String split[] = resultString.split(",");
		String splitId[] = split[3].split(":");
		datasource = Integer.parseInt(splitId[splitId.length -1]);
		
		MockMultipartFile file = new MockMultipartFile("file", "filename.csv", "text/csv", new FileInputStream(ptH +"src/test/resources/ingestdatasource/ANPR-Sample1.csv"));
		ResultActions resultImport = restController.perform(MockMvcRequestBuilders.fileUpload(Globals.REST_API_DATA_SOURCE + "/dataimport")
	              .file(file)
	               .param("datasource", ""+datasource)
					.param("name", "import1")
					.param("description", "test...")
					.param("userName", "test")
					.header("x-auth-username", "a")
			).andExpect(status().isOk());
		
		String resultImportData = resultImport.andReturn().getResponse().getContentAsString();
		System.out.println("DATA::" + resultImportData);
		Thread.sleep(5000);
	}
	
	/*@Test(description = "delete  datasource which is not in database which has data import" , dependsOnMethods = "TestAddingDataImport")
	public void TestDeleteDataSourceWithDataImport() throws Exception {
		ResultActions result = restController.perform(delete(Globals.REST_API_DATA_SOURCE+"/"+datasource).header("Content-Type", "application/json")).andExpect(status().is5xxServerError());
	}*/
	
	int dataimport = 0;
	@Test(description = "get  Data import", dependsOnMethods = "TestDeleteDataSourceNotInDB")
	public void TestgetAllDataImport() throws Exception {
		ObjectMapper obj_mapper =  new ObjectMapper();
		PaginationDTO pg_dto =  new PaginationDTO();
		pg_dto.setStartRow(0);
		pg_dto.setEndRow(80);
		String json = obj_mapper.writeValueAsString(pg_dto);
		ResultActions result = restController.perform(post(Globals.REST_API_DATA_SOURCE + "/dataimport/getAll").content(json).header("Content-Type", "application/json").header("x-auth-username", "a")).andExpect(status().isOk());
		String resultString = result.andReturn().getResponse().getContentAsString();
		System.out.println("DATA::" + resultString);
		String splitImport[] = resultString.split(",");
		String splitImportId[] = splitImport[0].split(":");
		dataimport = Integer.parseInt(splitImportId[splitImportId.length -1]);
	}

	@Test(description = "get single data import", dependsOnMethods = "TestgetAllDataImport")
	public void TestGetDataImport() throws Exception {
		ResultActions result = restController.perform(get(Globals.REST_API_DATA_SOURCE + "/dataimport"+"/"+dataimport).header("Content-Type", "application/json").header("x-auth-username", "a")).andExpect(status().isOk());
		String resultString = result.andReturn().getResponse().getContentAsString();
		System.out.println("DATA::" + resultString);
	}
	
	@Test(description = "get  data import for a datasource", dependsOnMethods = "TestgetAllDataImport")
	public void TestGetDataImportForDataSourceId() throws Exception {
		ObjectMapper obj_mapper =  new ObjectMapper();
		PaginationDTO pg_dto =  new PaginationDTO();
		pg_dto.setStartRow(0);
		pg_dto.setEndRow(80);
		String json = obj_mapper.writeValueAsString(pg_dto);
		ResultActions result = restController.perform(post(Globals.REST_API_DATA_SOURCE + "/"+datasource+"/dataimport").content(json).header("Content-Type", "application/json").header("x-auth-username", "a")).andExpect(status().isOk());
		String resultString = result.andReturn().getResponse().getContentAsString();
		System.out.println("DATA::" + resultString);
	}

	@Test(description = "delete  data import", dependsOnMethods = "TestGetDataImportForDataSourceId")
	public void TestDeleteDataImport() throws Exception {
		ResultActions result = restController.perform(delete(Globals.REST_API_DATA_SOURCE + "/dataimport"+"/"+dataimport).header("Content-Type", "application/json").header("x-auth-username", "a")).andExpect(status().isOk());
		String resultString = result.andReturn().getResponse().getContentAsString();
		Thread.sleep((elasticRefreshRate+scheduledWaitTime+5)*1000);
		System.out.println("DATA::" + resultString);
	}
	
	@Test(description = "get  dataimport which is not in database", dependsOnMethods = "TestDeleteDataImport")
	public void TestGetDataImportNotInDB() throws Exception {
		ResultActions result = restController.perform(get(Globals.REST_API_DATA_SOURCE + "/dataimport"+"/"+dataimport).header("Content-Type", "application/json").header("x-auth-username", "a")).andExpect(status().isOk());
		String resultString = result.andReturn().getResponse().getContentAsString();
		System.out.println("DATA::" + resultString);
		Assert.isTrue(resultString.contains("\"lastRow\":0"), "Checking no data returned");
	}

	@Test(description = "delete  dataimport which is not in database" , dependsOnMethods = "TestGetDataImportNotInDB")
	public void TestDeleteDataImportNotInDB() throws Exception {
		ResultActions result = restController.perform(delete(Globals.REST_API_DATA_SOURCE + "/dataimport"+"/"+dataimport).header("Content-Type", "application/json").header("x-auth-username", "a")).andExpect(status().is2xxSuccessful());
	}
	 
	@Test(description = "Add new Data Source eventtype" , dependsOnMethods = "TestDeleteDataImportNotInDB")
	public void TestAddingDataSourceEventType() throws Exception {
        String json = getFileContentAsString(ptH + "src/test/resources/ingestdatasource/json-datasource-eventtype.txt");
		ResultActions result = restController.perform(post(Globals.REST_API_DATA_SOURCE+"/eventtypes").content(json).header("Content-Type", "application/json").header("x-auth-username", "a")).andExpect(status().isOk());
		String resultString = result.andReturn().getResponse().getContentAsString();
		System.out.println("DATA::" + resultString);
	}

	@Test(description = "get  Data Source event types", dependsOnMethods = "TestAddingDataSourceEventType")
	public void TestgetAllDataSourceEventTypes() throws Exception {
		ResultActions result = restController.perform(get(Globals.REST_API_DATA_SOURCE+"/eventtypes").header("Content-Type", "application/json").header("x-auth-username", "a")).andExpect(status().isOk());
		String resultString = result.andReturn().getResponse().getContentAsString();
		System.out.println("DATA::" + resultString);
	}
	
	@Test(description = "get Data Source types", dependsOnMethods = "TestgetAllDataSourceEventTypes")
	public void TestgetAllDataSourceTypes() throws Exception {
		ResultActions result = restController.perform(get(Globals.REST_API_DATA_SOURCE+"/datasourcetypes").header("Content-Type", "application/json").header("x-auth-username", "a")).andExpect(status().isOk());
		String resultString = result.andReturn().getResponse().getContentAsString();
		System.out.println("DATA::" + resultString);
	}
	
	@AfterClass
	public void cleanup() throws Exception {
	}

	
}
