package com.ss8.enrichment.enrichmentservice;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.Reporter;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = EnrichmentApplication.class)
@AutoConfigureMockMvc
@EmbeddedKafka(partitions = 1, topics = { "FILE-PARSER-TOPIC", "ENRICHMENT-INGEST-TOPIC" })
@TestPropertySource(locations = "file:src/test/resources/application-test.properties")
public class EnrichmentApplicationTest extends AbstractTestNGSpringContextTests {
	
	public static String ptH;
	public static final boolean IS_WINDOWS = System.getProperty("os.name").contains("Windows");

	public static String home = ptH + "\\src\\test\\java\\com\\ss8\\enrichment\\enrichmentservice";
	public static String dataHomeConfig = home + "\\data\\config/";
	public static String dataHomeRequest = home + "\\data\\request/";
	
	@Autowired
	public MockMvc restController;

	public EnrichmentApplicationTest() {
		System.out.println("Constructing EnrichmentApplicationTest ...");
		
		EnvSetup.beChatty = true;
		ptH = EnvSetup.setSystemProps("enrichment-service");

		home = ptH + "\\src\\test\\java\\com\\ss8\\enrichment\\enrichmentservice";
		dataHomeConfig = home + "\\data\\config/";
		dataHomeRequest = home + "\\data\\request/";
	}
	
	public void copyFile(String from, String to) throws IOException {
		Path copied = Paths.get(to);
		Path originalPath = Paths.get(from);
		Files.copy(originalPath, copied, StandardCopyOption.REPLACE_EXISTING);
	}

	public static void setFinalStaticField(Class<?> classzz, String fieldName, Object value, int modifier)
			throws ReflectiveOperationException {

		Field field = classzz.getDeclaredField(fieldName);
		field.setAccessible(true);
		Field modifiers = Field.class.getDeclaredField("modifiers");
		modifiers.setAccessible(true);
		modifiers.setInt(field, field.getModifiers() & ~modifier);
		field.set(null, value);

	}
	
	public String getFileContentAsString(String filePath) {
		String content = null;
		StringBuilder sb = new StringBuilder();
		BufferedReader br = null;
		try {

			br = new BufferedReader(new FileReader(filePath));
			String line = br.readLine();
			while (line != null) {
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			content = sb.toString();
		}
		return content;
	}

	public void print(String line) {
		System.out.println(line);
		 Reporter.log(line);
	}

}
