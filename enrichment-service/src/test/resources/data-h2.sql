INSERT INTO `query_fields`(field, sourceNames, displayName ,fieldType,childType , collection) VALUES ('id','_id','id' ,'Text' , null , 'enrichment');
INSERT INTO `query_fields`(field, displayName ,fieldType,childType , collection) VALUES ('eventDate','Event Date' ,'DateTime' , null , 'enrichment');
INSERT INTO `query_fields`(field, displayName ,fieldType,childType , collection) VALUES ('ingestDate','Ingest Date' ,'DateTime' , null , 'enrichment');
INSERT INTO `query_fields`(field, displayName ,fieldType,childType , collection) VALUES ('datasourceId','Datasource Id' ,'Number' , null , 'enrichment');
INSERT INTO `query_fields`(field, displayName ,fieldType,childType , collection) VALUES ('datasourceName','Datasource Name' ,'Text' , null , 'enrichment');
INSERT INTO `query_fields`(field, displayName ,fieldType,childType , collection) VALUES ('importId','Import Id' ,'Number' , null , 'enrichment');
INSERT INTO `query_fields`(field, displayName ,fieldType,childType , collection) VALUES ('importName','Import Name' ,'Text' , null , 'enrichment');
INSERT INTO `query_fields`(field, displayName ,fieldType,childType , collection) VALUES ('intercept','Intercept Id' ,'Number' , null , 'enrichment');
INSERT INTO `query_fields`(field, displayName ,fieldType,childType , collection) VALUES ('globalSearchText','Global Search Text' ,'LongText' , null , 'enrichment');
INSERT INTO `query_fields`(field, displayName ,fieldType,childType , collection) VALUES ('eventLocations','Event Locations' ,'GeoPoint' , null , 'enrichment');


INSERT INTO `datasource_event_types`(id ,event_type) VALUES (1 ,'Web');
INSERT INTO `datasource_event_types`(id ,event_type) VALUES (2 ,'Email');
INSERT INTO `datasource_event_types`(id ,event_type) VALUES (3 ,'Attachment');
INSERT INTO `datasource_event_types`(id ,event_type) VALUES (4 ,'Chat');
INSERT INTO `datasource_event_types`(id ,event_type) VALUES (5 ,'IRI');
INSERT INTO `datasource_event_types`(id ,event_type) VALUES (6 ,'Call');
INSERT INTO `datasource_event_types`(id ,event_type) VALUES (7 ,'CDR');
INSERT INTO `datasource_event_types`(id ,event_type) VALUES (8 ,'Contact');
INSERT INTO `datasource_event_types`(id ,event_type) VALUES (9 ,'Inbox');
INSERT INTO `datasource_event_types`(id ,event_type) VALUES (10 ,'Call_AV');
INSERT INTO `datasource_event_types`(id ,event_type) VALUES (11 ,'Filexfer');
INSERT INTO `datasource_event_types`(id ,event_type) VALUES (12 ,'Social');
INSERT INTO `datasource_event_types`(id ,event_type) VALUES (13 ,'Certificate');
INSERT INTO `datasource_event_types`(id ,event_type) VALUES (14 ,'Protocol');
INSERT INTO `datasource_event_types`(id ,event_type) VALUES (15 ,'DNS');
INSERT INTO `datasource_event_types`(id ,event_type) VALUES (16 ,'Unknown_Protocol');
INSERT INTO `datasource_event_types`(id ,event_type) VALUES (17 ,'Web_Search');
INSERT INTO `datasource_event_types`(id ,event_type) VALUES (18 ,'Web_Map');

INSERT INTO `datasource_types`(id, datasource_type) VALUES (1, 'Regular');
INSERT INTO `datasource_types`(id, datasource_type) VALUES (2, 'BSA');

commit;